﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GestFilmes.DAL;
using GestFilmes.Libs;

namespace GestFilmes.Pages.FrontOffice.Movies
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private Access WebApi;
        private Filme filme;
        protected void Page_Load(object sender, EventArgs e)
        {
            User user = Session["logIn"] as User;

            //if (!WebHelper.isLoggedIn(user))
            //{
            //    Response.Redirect("~/Pages/Account/Login.aspx");
            //}

            if (!WebHelper.isLoggedIn(user, TipoUser.Gestor))
            {
                linkEraseMovie.Visible = false;
                linkEditMovie.Visible = false;
            }
            WebApi = WebHelper.CreateWebAPI();

            if (!Page.IsPostBack)
            {
                if (Request.QueryString["id"] == null || !WebApi.filmeExists(Request.QueryString["id"]))
                {
                    Response.Redirect("~/Pages/FrontOffice/Dashboard.aspx");
                }
                else
                {
                    CarregarAtributos();
                    if (Session["logIn"] == null)
                    {
                        formComentario.Visible = false;
                    }
                }
                if(user!=null)
                {
                    int? avaliacao = WebApi.getAvaliacao(Request.QueryString["id"], user.Username);
                    if (avaliacao != null)
                    {
                        txtRating.Text = avaliacao.ToString();
                    }
                }
            }
            double? avgRating = WebApi.getAvaliacaoAvg(Request.QueryString["id"]);
            ratingResult.InnerHtml = $"<h3>Rating : {(avgRating == null ? "Unavailable" : avgRating.ToString())}</h3>";
            filme = WebApi.getFilme(Request.QueryString["id"]);
            linkEditMovie.InnerHtml = "<a href='../../BackOffice/Movies/Edit.aspx?id=" + filme.ID + "' class='btn btn-outline-warning'>Editar Filme</a>";
            CarregarComentarios();
        }

        private void CarregarComentarios()
        {
            List<Comentario> comentarios = WebApi.getComentariosFromFilme(Request.QueryString["id"]);
            comentariosDoFilme.InnerHtml = "";
            foreach (Comentario comentario in comentarios)
            {
                comentariosDoFilme.InnerHtml += "<div class='card mb-3'>";
                comentariosDoFilme.InnerHtml += $"<div class='card-header'>{comentario.Username} - {comentario.created_at}</div>";
                comentariosDoFilme.InnerHtml += $"<div class='card-body'>{comentario.Conteudo}</div></div>";
            }
        }

        private void CarregarAtributos()
        {
            filme = WebApi.getFilme(Request.QueryString["id"]);
            mvi.InnerHtml = "<br/><div class='row'><div class='col-md-8'><h2><span class='text-muted'>Título:</span> " + filme.Titulo + "<h2/><h2><span class='text-muted'>Resumo:</span> " + filme.Resumo + "<h2/></div><div class='col-md-4'><img src='../../../Content/img/" + filme.Image + "' style='max-width:250px;' class='img-fluid float-right'/></div></div>";

            var estudios = filme.Estudios;
            if (estudios != null)
            {
                estudiosdoFilme.InnerHtml = "<h1>Estúdios:</h1><hr/>";
                estudiosdoFilme.InnerHtml += "<ul>";
                foreach (var estudio in estudios)
                {
                    estudiosdoFilme.InnerHtml += "<li><a href='#'>" + estudio.Nome + "</a></li><br/>";
                }
                estudiosdoFilme.InnerHtml += "</ul>";

            }
            var atores = filme.Atores;
            if (atores != null)
            {
                atoresDoFilme.InnerHtml += "<h1>Atores:</h1><hr/>";
                atoresDoFilme.InnerHtml += "<ul>";
                foreach (var ator in atores)
                {
                    atoresDoFilme.InnerHtml += "<li><a href='#'>" + ator.Nome + "</a></li><br/>";
                }
                atoresDoFilme.InnerHtml += "</ul>";
            }

            var genero = filme.Genero;
            if (genero != null)
            {
                generoDoFilme.InnerHtml += "<h1>Género:</h1><hr/>";
                generoDoFilme.InnerHtml += "<ul>";
                generoDoFilme.InnerHtml += "<li><a href='#'>" + genero.Nome + "</a></li><br/>";
                generoDoFilme.InnerHtml += "</ul>";
            }

            var realizadores = filme.Realizadores;
            if (realizadores != null)
            {
                realizadoresDoFilme.InnerHtml += "<h1>Realizadores:</h1><hr/>";
                realizadoresDoFilme.InnerHtml += "<ul>";
                foreach (var realizador in realizadores)
                {
                    realizadoresDoFilme.InnerHtml += "<li><a href='#'>" + realizador.Nome + "</a></li><br/>";
                }
                realizadoresDoFilme.InnerHtml += "</ul>";
            }

            var produtores = filme.Produtores;
            if (produtores != null)
            {
                produtoresDoFilme.InnerHtml += "<h1>Produtores:</h1><hr/>";
                produtoresDoFilme.InnerHtml += "<ul>";
                foreach (var produtor in produtores)
                {
                    produtoresDoFilme.InnerHtml += "<li><a href='#'>" + produtor.Nome + "</a></li><br/>";
                }
                produtoresDoFilme.InnerHtml += "</ul>";
            }
        }

        protected void btnAddComment_Click(object sender, EventArgs e)
        {
            if (validarComment())
            {
                User user = Session["logIn"] as User;
                if (user == null)
                    Response.Redirect("~/Pages/FrontOffice/Dashboard.aspx");
                if (WebApi.addComentario(filme.ID, user.Username, txtComentario.Text))
                {
                    comentariosDoFilme.InnerHtml += "<div class='card mb-3'>";
                    comentariosDoFilme.InnerHtml += $"<div class='card-header'>{user.Username} - Agora</div>";
                    comentariosDoFilme.InnerHtml += $"<div class='card-body'>{txtComentario.Text}</div></div>";
                    txtComentario.Text = "";
                }
            }
        }

        private bool validarComment()
        {
            return !string.IsNullOrEmpty(txtComentario.Text);
        }

        protected void btnSaveRating_Click(object sender, EventArgs e)
        {
            int result = -1;
            User user = Session["logIn"] as User;
            if (txtRating.Text != null)
            {
                if (int.TryParse(txtRating.Text, out result))
                {
                    if (result >= 0 && result <= 5)
                    {
                        WebApi.addAvaliacao(filme.ID, user.Username, result);
                    }
                }
            }
        }

        protected void btnDeleteMovie(object sender, EventArgs e)
        {
            WebApi = WebHelper.CreateWebAPI();
            WebApi.deleteFilme(filme);
            Response.Redirect("~/Pages/FrontOffice/Dashboard.aspx");
        }
    }
}