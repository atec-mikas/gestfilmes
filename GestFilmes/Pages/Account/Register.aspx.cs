﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GestFilmes.DAL;
using GestFilmes.Libs;

namespace GestFilmes.Pages.Account
{
    public partial class Register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["logIn"] != null)
            {
                Response.Redirect("~/Pages/FrontOffice/Dashboard.aspx");
            }
        }
        
        protected void CreateUser_Click(object sender, EventArgs e)
        {


            Access WebApi = WebHelper.CreateWebAPI();
            if (WebApi.userExists(Username.Text))
            {
                Response.Write("Username is taken, please choose another one!");
            }
            else
            {
                Libs.User user = new Libs.User() { Username = Username.Text, Password = Password.Text, TipoUser = Libs.TipoUser.Registado };
                if (WebApi.saveUser(user))
                {
                    Session["logIn"] = user;
                    Response.Redirect("~/Pages/FrontOffice/Dashboard.aspx");
                }

                   
            }
            
            
            
        } 
        
    }
}