﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GestFilmes.DAL;
using GestFilmes.Libs;
using System.IO;

namespace GestFilmes.Pages.BackOffice.Movies
{
    public partial class Create : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            Access WebApi = WebHelper.CreateWebAPI();
            User usr;
            usr = Session["logIn"] as User;
            if (Session["logIn"] == null)
            {
                Response.Redirect("~/Pages/Account/Login.aspx");
            }
            if (usr.TipoUser == TipoUser.Registado && Session["logIn"] != null)
            {
                Response.Redirect("~/Pages/Account/Login.aspx");
            }
            if (usr.TipoUser == TipoUser.Gestor && Session["logIn"] != null)
            {

            }
            if (!IsPostBack)
            {


            List<Ator> atores = WebApi.getAllAtores();
            DropDownListAtores.DataValueField = "ID";
            DropDownListAtores.DataTextField = "Nome";
            DropDownListAtores.DataSource = atores;
            DropDownListAtores.DataBind();


            List<Estudio> estudios = WebApi.getAllEstudios();
            DropDownListEstudios.DataValueField = "ID";
            DropDownListEstudios.DataTextField = "Nome";
            DropDownListEstudios.DataSource = estudios;
            DropDownListEstudios.DataBind();


            List<Genero> generos = WebApi.getAllGeneros();
            DropDownListGeneros.DataValueField = "ID";
            DropDownListGeneros.DataTextField = "Nome";
            DropDownListGeneros.DataSource = generos;
            DropDownListGeneros.DataBind();
            //foreach (Genero genero in generos)
            //{
            //    DropDownListGeneros.Items.Add(genero);
            //}


            List<Produtor> produtores = WebApi.getAllProdutores();
            DropDownListProdutores.DataValueField = "ID";
            DropDownListProdutores.DataTextField = "Nome";
            DropDownListProdutores.DataSource = produtores;
            DropDownListProdutores.DataBind();

            List<Realizador> realizadores = WebApi.getAllRealizadores();
            DropDownListRealizadores.DataValueField = "ID";
            DropDownListRealizadores.DataTextField = "Nome";
            DropDownListRealizadores.DataSource = realizadores;
            DropDownListRealizadores.DataBind();
            }
            
        }

        protected void addMovie(object sender, EventArgs e)
        {
            string fileName = "";
            // #PUREMAGIC @peleyah
            if (FileUpload1.HasFile)
            {
                if (!File.Exists(Server.MapPath("~/Content/img/" + FileUpload1.FileName)))
                {
                   fileName = FileUpload1.FileName;
                }
                else
                {
                    string fileNameWithoutExt = Guid.NewGuid().ToString();
                    fileName = fileNameWithoutExt + Path.GetExtension(FileUpload1.FileName);
                }
                FileUpload1.SaveAs(Server.MapPath("~/Content/img/" + fileName));
            }
            Access WebApi = WebHelper.CreateWebAPI();
            Filme filme = new Filme();
            filme.Image = fileName;
            filme.Titulo = Title.Text;
            filme.Resumo = Resume.Text;
            Genero genero = new Genero();
            genero = WebApi.getGenero(DropDownListGeneros.SelectedValue);
            filme.Genero = genero;
            WebApi.saveFilme(filme);
            foreach (ListItem item in DropDownListAtores.Items)
            {
                if (item.Selected)
                {
                    Ator ator = new Ator();
                    ator = WebApi.getAtor(item.Value);
                    WebApi.addAtor(filme, ator);
                }

            }
            foreach (ListItem item in DropDownListEstudios.Items)
            {
                if (item.Selected)
                {
                    Estudio estudio = new Estudio();
                    estudio = WebApi.getEstudio(item.Value);
                    WebApi.addEstudio(filme, estudio);
                }

            }

            foreach (ListItem item in DropDownListProdutores.Items)
            {
                if (item.Selected)
                {
                    Produtor produtor = new Produtor();
                    produtor = WebApi.getProdutor(item.Value);
                    WebApi.addProdutor(filme, produtor);
                }

            }
            foreach (ListItem item in DropDownListRealizadores.Items)
            {
                if (item.Selected)
                {
                    Realizador realizador = new Realizador();
                    realizador = WebApi.getRealizador(item.Value);
                    WebApi.addRealizador(filme, realizador);
                }

            }

            Response.Redirect("~/Pages/FrontOffice/Dashboard.aspx");
        }
    }
}