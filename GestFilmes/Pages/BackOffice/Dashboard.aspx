﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/BackOffice.Master" CodeBehind="Dashboard.aspx.cs" Inherits="GestFilmes.Pages.BackOffice.WebForm1" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <div id="links" runat="server" class="mx-auto mt-4 " visible="false">
        <div class="row">
            <div class="col-md-2">
                <asp:HyperLink runat='server' NavigateUrl='~/Pages/BackOffice/Movies/Create.aspx' CssClass='btn btn-outline-secondary'>🎥 Adicionar novo filme</asp:HyperLink>
            </div>
            <div class="col-md-2">
                <asp:HyperLink runat='server' NavigateUrl='~/Pages/BackOffice/Estudios/Index.aspx' CssClass='btn btn-outline-secondary'>🏛️ Gerir Estúdios</asp:HyperLink>
            </div>
            <div class="col-md-2">
                <asp:HyperLink runat='server' NavigateUrl='~/Pages/BackOffice/Atores/Index.aspx' CssClass='btn btn-outline-secondary'>🤨 Gerir Atores</asp:HyperLink>
            </div>
            <div class="col-md-2">
                <asp:HyperLink runat='server' NavigateUrl='~/Pages/BackOffice/Generos/Index.aspx' CssClass='btn btn-outline-secondary'>⚙️ Gerir Géneros</asp:HyperLink>
            </div>
            <div class="col-md-2">
                <asp:HyperLink runat='server' NavigateUrl='~/Pages/BackOffice/Produtores/Index.aspx' CssClass='btn btn-outline-secondary'>⚙️ Gerir Produtores</asp:HyperLink>
            </div>
            <div class="col-md-2">
                <asp:HyperLink runat='server' NavigateUrl='~/Pages/BackOffice/Realizadores/Index.aspx' CssClass='btn btn-outline-secondary'>💺 Gerir Realizadores</asp:HyperLink>
            </div>


        </div>
        <div class="row mt-2">
            <div class="col-md-12 text-center">
                <asp:HyperLink runat='server' NavigateUrl='~/Pages/BackOffice/Contas/Index.aspx' CssClass='btn btn-outline-secondary'>⚙️ Gerir Contas</asp:HyperLink>
            </div>

        </div>
        <hr />
    </div>
</asp:Content>
