﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GestFilmes.DAL;
using GestFilmes.Libs;

namespace GestFilmes.Pages.BackOffice.Generos
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            Access WebApi = WebHelper.CreateWebAPI();
            User usr = Session["logIn"] as User;
            if (Session["logIn"] == null)
            {
                Response.Redirect("~/Pages/Account/Login.aspx");
            }
            if (!WebHelper.isLoggedIn(usr))
            {
                Response.Redirect("~/Pages/FrontOffice/Dashboard.aspx");
            }
            var genres = WebApi.getAllGeneros();


            generos.InnerHtml = "<ul>";
            foreach (var item in genres)
            {
                generos.InnerHtml += "<li><a href='Show.aspx?id=" + item.ID + "'>" + item.Nome + "</a></li><hr/>";
            }
            generos.InnerHtml += "</ul>";

        }



    }
}