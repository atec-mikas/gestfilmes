﻿<%@ Page Language="C#"  AutoEventWireup="true" MasterPageFile="~/BackOffice.Master" CodeBehind="Edit.aspx.cs" Inherits="GestFilmes.Pages.BackOffice.Movies.Edit" %>


<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <div class="container-fluid mt-5">
        <div class="row">
            <div class="col-md-8 mx-auto">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Edit Movie</h5>
                        <h4>Edit movie here.</h4>
                        <hr />
                        <asp:ValidationSummary runat="server" CssClass="text-danger" />
                        <section id="addMovieForm">
                            <div class="form-horizontal">
                                <asp:PlaceHolder runat="server" ID="ErrorMessage" Visible="false">
                                    <p class="text-danger">
                                        <asp:Literal runat="server" ID="FailureText" />
                                    </p>
                                </asp:PlaceHolder>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="Title" CssClass="col-md-2 control-label">Title</asp:Label>
                                    <div class="col-md-12">
                                        <asp:TextBox runat="server" ID="Title" CssClass="form-control" />
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="Title"
                                            CssClass="text-danger" ErrorMessage="The title field is required." />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="col-md-2 control-label">Image</asp:Label>
                                    <div class="col-md-12">
                                        <asp:FileUpload runat="server" CssClass="form-control" ID="FileUpload1" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="Resume" CssClass="col-md-2 control-label">Resume</asp:Label>
                                    <div class="col-md-12">
                                        <asp:TextBox runat="server" ID="Resume" CssClass="form-control" TextMode="MultiLine" />
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="Resume"
                                            CssClass="text-danger" ErrorMessage="The resume field is required." />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="col-md-2 control-label">Atores</asp:Label>
                                    <div class="col-md-12">
                                        <asp:ListBox runat="server" ID="DropDownListAtores" SelectionMode="Multiple" CssClass="form-control"></asp:ListBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="col-md-2 control-label">Estudios</asp:Label>
                                    <div class="col-md-12">
                                        <asp:ListBox runat="server" ID="DropDownListEstudios" SelectionMode="Multiple"  CssClass="form-control"></asp:ListBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="col-md-2 control-label">Generos</asp:Label>
                                    <div class="col-md-12">
                                        <asp:DropDownList runat="server" ID="DropDownListGeneros" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="col-md-2 control-label">Produtores</asp:Label>
                                    <div class="col-md-12">
                                        <asp:ListBox runat="server" ID="DropDownListProdutores" SelectionMode="Multiple" CssClass="form-control"></asp:ListBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="col-md-2 control-label">Realizadores</asp:Label>
                                    <div class="col-md-12">
                                        <asp:ListBox runat="server" ID="DropDownListRealizadores" SelectionMode="Multiple" CssClass="form-control"></asp:ListBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-2 col-md-10">
                                    <asp:Button runat="server" Text="Edit movie" CssClass="btn btn-outline-primary" OnClick="Unnamed12_Click" 
                                        />
                                </div>
                            </div>
                        </section>
                    </div>

                </div>
            </div>
        </div>
    </div>

</asp:Content>

