﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/BackOffice.Master" CodeBehind="Create.aspx.cs" Inherits="GestFilmes.Pages.BackOffice.Generos.Create" %>


<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <div class="container-fluid mt-5">
        <div class="row">
            <div class="col-md-8 mx-auto">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Add Genre</h5>
                        <h4>Add new Genre here.</h4>
                        <hr />
                        <asp:ValidationSummary runat="server" CssClass="text-danger" />
                        <section id="addGenreForm">
                            <div class="form-horizontal">
                                <asp:PlaceHolder runat="server" ID="ErrorMessage" Visible="false">
                                    <p class="text-danger">
                                        <asp:Literal runat="server" ID="FailureText" />
                                    </p>
                                </asp:PlaceHolder>
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="Name" CssClass="col-md-2 control-label">Name</asp:Label>
                                    <div class="col-md-12">
                                        <asp:TextBox runat="server" ID="Name" CssClass="form-control" />
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="Name"
                                            CssClass="text-danger" ErrorMessage="The genre name field is required." />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10">
                                        <asp:Button runat="server" Text="Add new genre" CssClass="btn btn-outline-primary" OnClick="addGenre" />
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
