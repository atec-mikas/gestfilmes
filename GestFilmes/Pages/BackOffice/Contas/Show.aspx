﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BackOffice.Master" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="GestFilmes.Pages.BackOffice.Contas.Show" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div ID="Dcontas" runat="server" class="contas">
         <div class="container mt-5">
            <div class="card">
                <div class="card-body">
                    <div ID="DadosConta" runat="server" class="conta">
                    </div>
                        <div class="float-right">
                            <asp:LinkButton runat="server" ID="DeleteConta" OnClick="DeleteConta_Click" Text="Apagar Conta" CssClass="btn btn-outline-danger"></asp:LinkButton>
                            <asp:LinkButton runat="server" ID="Switcharoo" OnClick="Switcharoo_Click" ></asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
         </div>
    </div>

</asp:Content>
