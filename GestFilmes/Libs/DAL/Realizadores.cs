﻿using GestFilmes.Libs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace GestFilmes.DAL
{
    internal static class Realizadores
    {
        private static DAL dal = new DAL("Realizador", Properties.Settings.Default.DBPath);

        internal static Realizador GetRealizador(string id)
        {
            Realizador realizador = null;
            DataTable result = dal.find($"id,nome", $"where id = '{SQLHelper.EscapeSQL(id)}'");
            if (result.Rows.Count > 0)
            {
                realizador = new Realizador(result.Rows[0]);
            }
            return realizador;
        }

        internal static List<Realizador> GetAllRealizadores()
        {
            List<Realizador> realizadores = new List<Realizador>();
            DataTable result = dal.get($"id,nome");
            if (result.Rows.Count > 0)
            {
                foreach (DataRow realizador in result.Rows)
                    realizadores.Add(new Realizador(realizador));
            }
            return realizadores;
        }

        internal static bool RealizadorExists(string id)
        {
            return dal.exists($"where id = '{SQLHelper.EscapeSQL(id)}'");
        }

        internal static bool SaveRealizador(ref Realizador realizador)
        {
            List<KeyValuePair<string, object>> kv = new List<KeyValuePair<string, object>>();
            kv.Add(new KeyValuePair<string, object>("nome", realizador.Nome));
            bool status = true;
            try
            {
                if (!RealizadorExists(realizador.ID))
                {
                    string id;
                    do
                    {
                        id = Guid.NewGuid().ToString();
                    } while (RealizadorExists(id));

                    kv.Add(new KeyValuePair<string, object>("id", id));
                    status = dal.insert(kv);
                    if (status)
                        realizador.ID = id;
                }
                else
                {
                    status = dal.update(kv, $"where id='{SQLHelper.EscapeSQL(realizador.ID)}'");
                }
            }
            catch
            {
                status = false;
            }
            return status;
        }

        internal static bool DeleteRealizador(Realizador realizador)
        {
            DAL dalFilmeRealizador = new DAL("Filme_Realizador", Properties.Settings.Default.DBPath);
            return dalFilmeRealizador.delete($"where realizador_id='{SQLHelper.EscapeSQL(realizador.ID)}'") 
                    && dal.delete($"where id='{SQLHelper.EscapeSQL(realizador.ID)}'");
        }
    }
}