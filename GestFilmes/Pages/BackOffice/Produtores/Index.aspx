﻿<%@ Page Title="Dashboard" Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs"
    Inherits="GestFilmes.Pages.BackOffice.Produtores.Index" MasterPageFile="~/BackOffice.Master"%>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <div ID="linkCreateProducer" runat="server" class="mx-auto mt-4" visible="true" style="width:200px;">
        <asp:HyperLink runat='server' NavigateUrl='~/Pages/BackOffice/Produtores/Create.aspx' CssClass='btn btn-outline-secondary'>+ Adicionar novo produtor</asp:HyperLink>
    </div>
    <hr />
         <div class="container mt-5">
             <div class="card">
                 <div class="card-body">
                     <h5 class="card-title">Lista de Produtores</h5>
                     <div ID="produtores" runat="server" class="produtores">
                     </div>
                 </div>
             </div>
         </div>
</asp:Content>
