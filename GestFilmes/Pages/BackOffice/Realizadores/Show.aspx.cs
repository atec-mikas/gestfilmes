﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GestFilmes.DAL;
using GestFilmes.Libs;

namespace GestFilmes.Pages.BackOffice.Realizadores
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private Access WebApi;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            WebApi = WebHelper.CreateWebAPI();
           
            User usr = Session["logIn"] as User;
            if (!WebHelper.isLoggedIn(usr, TipoUser.Gestor))
            {
                Response.Redirect("~/Pages/Account/Login.aspx");
            }
            if (!WebHelper.isLoggedIn(usr))
            {
                Response.Redirect("~/Pages/FrontOffice/Dashboard.aspx");
            }
            if (Request.QueryString["id"] == null || WebApi.getRealizador(Request.QueryString["id"])==null)
            {
                Response.Redirect("~/Pages/BackOffice/Dashboard.aspx");
            }
            Realizador realizador = WebApi.getRealizador(Request.QueryString["id"]);
            nomeRealizador.InnerHtml = "<span>Realizador número: " + realizador.ID + "<br> Nome Realizador: " + realizador.Nome + "</span>"; 
        }
        protected void deleteRealizador(object sender, EventArgs e)
        {
            Realizador realizador = WebApi.getRealizador(Request.QueryString["id"]);
            if(WebApi.deleteRealizador(realizador) == true)
            {
                Response.Redirect("~/Pages/BackOffice/Realizadores/Index.aspx");
                Response.Write("Realizador " + realizador.Nome + " eliminado com sucesso!");
            }else
            {
                Response.Write("Erro ao apagar realizador!");
            }

        }
    }
}