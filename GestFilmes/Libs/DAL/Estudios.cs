﻿using GestFilmes.Libs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace GestFilmes.DAL
{
    internal static class Estudios
    {
        private static DAL dal = new DAL("Estudio", Properties.Settings.Default.DBPath);

        internal static Estudio GetEstudio(string id)
        {
            Estudio estudio= null;
            DataTable result = dal.find($"id,nome", $"where id = '{SQLHelper.EscapeSQL(id)}'");
            if (result.Rows.Count > 0)
            {
                estudio = new Estudio(result.Rows[0]);
            }
            return estudio;
        }

        internal static List<Estudio> GetAllEstudios()
        {
            List<Estudio> estudios = new List<Estudio>();
            DataTable result = dal.get($"id,nome");
            if (result.Rows.Count > 0)
            {
                foreach (DataRow estudio in result.Rows)
                    estudios.Add(new Estudio(estudio));
            }
            return estudios;
        }

        internal static bool EstudioExists(string id)
        {
            return dal.exists($"where id = '{SQLHelper.EscapeSQL(id)}'");
        }

        internal static bool SaveEstudio(ref Estudio estudio)
        {
            List<KeyValuePair<string, object>> kv = new List<KeyValuePair<string, object>>();
            kv.Add(new KeyValuePair<string, object>("nome", estudio.Nome));
            bool status = true;
            try
            {
                if (!EstudioExists(estudio.ID))
                {
                    string id;
                    do
                    {
                        id = Guid.NewGuid().ToString();
                    } while (EstudioExists(id));

                    kv.Add(new KeyValuePair<string, object>("id", id));
                    status = dal.insert(kv);
                    if (status)
                        estudio.ID = id;
                }
                else
                {
                    status = dal.update(kv, $"where id='{SQLHelper.EscapeSQL(estudio.ID)}'");
                }
            }
            catch
            {
                status = false;
            }
            return status;
        }

        internal static bool DeleteEstudio(Estudio estudio)
        {
            DAL dalFilmeEstudio = new DAL("Filme_Estudio", Properties.Settings.Default.DBPath);
            return dalFilmeEstudio.delete($"where estudio_id='{SQLHelper.EscapeSQL(estudio.ID)}'")
                    && dal.delete($"where id='{SQLHelper.EscapeSQL(estudio.ID)}'");
        }
    }
}