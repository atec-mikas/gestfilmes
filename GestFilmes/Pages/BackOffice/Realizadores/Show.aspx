﻿<%@ Page Language="C#" MasterPageFile="~/BackOffice.Master" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="GestFilmes.Pages.BackOffice.Realizadores.WebForm1" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <div ID="directors" runat="server" class="directors">
        <div class="container mt-5">
            <div class="card">
                <div class="card-body">
                    <div ID="nomeRealizador" runat="server" class="nomeRealizador">
                    </div>
                    <div ID="Div1" runat="server" class="nomeRealizador mx-auto">
                        <div class="float-right">
                            <asp:Button runat="server" OnClick="deleteRealizador" CssClass="btn btn-outline-danger" id="deleteRealizadorButton" Text="Apagar Realizador"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>