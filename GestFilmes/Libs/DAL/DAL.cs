﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Web;

namespace GestFilmes.DAL
{
    internal class DAL
    {
        private OleDbConnection _conexao;
        private string _tabela;

        private bool openConnection(bool loop=true)
        {
            bool falhou = false;
            if (_conexao.State != System.Data.ConnectionState.Open)
            {
                bool connected;
                do
                {
                    connected = true;
                    try
                    {
                        _conexao.Open();
                    }
#pragma warning disable CS0168 // Variable is declared but never used
                    catch (Exception e)
#pragma warning restore CS0168 // Variable is declared but never used
                    {
                        falhou = true;
                        connected = false;
                    }
                    if (!connected && loop)
                        System.Threading.Thread.Sleep(100);
                } while (!connected && loop);
            }
            return !falhou;
        }

        private bool closeConnection()
        {
            bool result = true;
            try
            {
                _conexao.Close();
            }
             catch
            {
                result = false;
            }
            return result;
        }

        /// <summary>
        /// Função para dar escape a alguns caracteres
        /// </summary>
        /// <param name="s">String a dar escape</param>
        /// <returns>String escapada</returns>
        public static string EscapeSQL(string s)
        {
            return s.Replace("'", "[']");
        }

        public DAL(string path)
        {
            _conexao = new OleDbConnection();
            _conexao.ConnectionString = $"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={path};";
            _tabela = null;
            if (!openConnection(false))
            {
                throw new Exception("Invalid Connection String");
            }
            closeConnection();
        }

        /// <summary>
        /// Construtor para a DAL
        /// </summary>
        /// <param name="tabela">Nome da tablea</param>
        public DAL(string tabela,string path)
        {
            _conexao = new OleDbConnection();
            _conexao.ConnectionString = $"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={path};";
            _tabela = tabela;
            if(!openConnection(false))
            {
                throw new Exception("Invalid Connection String");
            }
            closeConnection();
        }

        /// <summary>
        /// Função equivalente a um select sem parametros adicionais
        /// </summary>
        /// <param name="s">String com o que quer ir buscar à tabela</param>
        /// <returns>OleDbDataReader com os dados</returns>
        public DataTable get(string s)
        {
            openConnection();


            OleDbCommand comando = new OleDbCommand();
            comando.CommandText = $"SELECT {s} FROM [{_tabela}]";

            comando.Connection = _conexao;
            OleDbDataReader t = comando.ExecuteReader();
            //_conexao.Close();
            DataTable result = new DataTable();
            result.Load(t);
            closeConnection();
            return result;
        }

        /// <summary>
        /// Função equivalente a um select com parametros adicionais
        /// </summary>
        /// <param name="s">String com o que quer ir buscar à tabela</param>
        /// <param name="a">String com os parametros adicionais assim como um "where"</param>
        /// <returns>OleDbDataReader com os dados</returns>
        public DataTable find(string s, string a)
        {
            openConnection();

            OleDbCommand comando = new OleDbCommand();
            comando.CommandText = $"SELECT {s} FROM [{_tabela}] {a}";

            comando.Connection = _conexao;
            OleDbDataReader t = comando.ExecuteReader();

            DataTable result = new DataTable();
            result.Load(t);
            closeConnection();
            return result;
        }

        public DataTable DirectQuery(string s)
        {
            openConnection();

            OleDbCommand comando = new OleDbCommand();
            comando.CommandText = s;

            comando.Connection = _conexao;
            OleDbDataReader t = comando.ExecuteReader();

            DataTable result = new DataTable();
            result.Load(t);
            closeConnection();
            return result;
        }

        /// <summary>
        /// Função equivalente a um insert
        /// </summary>
        /// <param name="kv">Lista de KeyValuePair em que a Key corresponde à coluna e o Value ao valor a adicionar</param>
        /// <returns>Boolean do Sucesso da operação</returns>
        public bool insert(List<KeyValuePair<string, object>> kv)
        {
            bool falhou = false;
            openConnection();

            string id = string.Empty;
            string valor = string.Empty;
            for (int i = 0; i < kv.Count; i++)
            {
                id += $"[{kv[i].Key}]";
                if(kv[i].Value.GetType()==typeof(int))
                    valor += kv[i].Value;
                else
                    valor += $"'{SQLHelper.EscapeSQL(kv[i].Value as string)}'";
                if (i != kv.Count - 1)
                {
                    id += ",";
                    valor += ",";
                }

            }


            OleDbCommand comando = new OleDbCommand();
            comando.Connection = _conexao;
            comando.CommandText = $"INSERT INTO [{_tabela}]({id}) VALUES ({valor})";
            try
            {
                comando.ExecuteNonQuery();
            }
#pragma warning disable CS0168 // Variable is declared but never used
            catch (Exception e)
#pragma warning restore CS0168 // Variable is declared but never used
            {
                falhou = true;
            }
            closeConnection();

            return !falhou;
        }

        /// <summary>
        /// Função equivalente a um update
        /// </summary>
        /// <param name="kv">Lista de KeyValuePair em que a Key corresponde à coluna e o Value ao valor a adicionar</param>
        /// <param name="a">Query que determina o que vai ser afetado Exemplo: "where id==1"</param>
        /// <returns>Boolean do Sucesso da operação</returns>
        public bool update(List<KeyValuePair<string, object>> kv, string a)
        {
            bool falhou = false;
            openConnection();

            string set = string.Empty;
            for (int i = 0; i < kv.Count; i++)
            {
                if (kv[i].Value.GetType() == typeof(int))
                    set += $"[{kv[i].Key}] = {kv[i].Value}";
                else
                    set += $"[{kv[i].Key}] = '{SQLHelper.EscapeSQL(kv[i].Value as string)}'";
                if (i != kv.Count - 1)
                {
                    set += ",";
                }
            }


            OleDbCommand comando = new OleDbCommand();
            comando.Connection = _conexao;
            comando.CommandText = $"UPDATE [{_tabela}] SET {set} {a}";
            try
            {
                comando.ExecuteNonQuery();
            }
#pragma warning disable CS0168 // Variable is declared but never used
            catch (Exception e)
#pragma warning restore CS0168 // Variable is declared but never used
            {
                falhou = true;
            }
            closeConnection();

            return !falhou;

        }

        /// <summary>
        /// Função equivalente a um delete
        /// </summary>
        /// <param name="a">Query que determina o que vai ser apagado Exemplo: "where id==1"</param>
        /// <returns>Boolean do Sucesso da operação</returns>
        public bool delete(string a)
        {
            bool falhou = false;
            openConnection();

            OleDbCommand comando = new OleDbCommand();
            comando.Connection = _conexao;
            comando.CommandText = $"delete * from [{_tabela}] {a}";
            try
            {
                comando.ExecuteNonQuery();
            }
#pragma warning disable CS0168 // Variable is declared but never used
            catch (Exception e)
#pragma warning restore CS0168 // Variable is declared but never used
            {
                falhou = true;
            }
            closeConnection();

            return !falhou;

        }

        /// <summary>
        /// Função que verifica se existe na base de dados
        /// </summary>
        /// <param name="a">Query que determina o que é verificado Exemplo: "where id==1"</param>
        /// <returns>Boolean de existência</returns>
        public bool exists(string a)
        {
            openConnection();


            OleDbCommand comando = new OleDbCommand();
            comando.Connection = _conexao;
            comando.CommandText = $"SELECT * FROM [{_tabela}] {a}";
            OleDbDataReader t = comando.ExecuteReader();
            bool result = t.HasRows;
            closeConnection();
            return result;
        }




    }
}