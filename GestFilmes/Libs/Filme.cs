﻿using GestFilmes.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace GestFilmes.Libs
{
    public class Filme
    {
        public string ID { get; set; }
        public string Titulo { get; set; }
        public Genero Genero { get; set; }
        public string Resumo { get; set; }
        public string Image { get; set; }
        public List<Estudio> Estudios
        {
            get
            {
                Access WebApi = WebHelper.CreateWebAPI();
                return WebApi.getEstudiosFromFilme(this.ID);
            }
        }
        public List<Realizador> Realizadores
        {
            get
            {
                Access WebApi = WebHelper.CreateWebAPI();
                return WebApi.getRealizadoresFromFilme(this.ID);
            }
        }
        public List<Produtor> Produtores
        {
            get
            {
                Access WebApi = WebHelper.CreateWebAPI();
                return WebApi.getProdutoresFromFilme(this.ID);
            }
        }
        public List<Ator> Atores
        {
            get
            {
                Access WebApi = WebHelper.CreateWebAPI();
                return WebApi.getAtoresFromFilme(this.ID);
            }
        }

        public Filme()
        {
            ID = String.Empty;
            Titulo = String.Empty;
            Genero = null;
            Resumo = String.Empty;
            Image = String.Empty;
        }

        public Filme(DataRow data)
        {
            ID = data["id"] as string ?? "";
            Titulo = data["titulo"] as string ?? "";
            Access WebApi = WebHelper.CreateWebAPI();
            Genero = WebApi.getGenero(data["genero_id"] as string);
            Resumo = data["resumo"] as string ?? "";
            Image = data["image"] as string ?? "";
        }


    }
}