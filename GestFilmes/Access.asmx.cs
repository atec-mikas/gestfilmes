﻿using GestFilmes.Libs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace GestFilmes.DAL
{
    /// <summary>
    /// Summary description for Access
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Access : System.Web.Services.WebService
    {

        private bool isLoggedIn()
        {
            return Session["Auth"] != null && AuthKey.AuthKeyExists(Session["Auth"] as string);
        }

        [WebMethod(EnableSession = true)]
        public bool logIn(string Auth)
        {
            Session["Auth"] = Auth;
            return isLoggedIn();
        }

        #region Users
        [WebMethod(EnableSession = true)]
        public User getUser(string username)
        {
            if(isLoggedIn())
            {
                return Users.GetUser(username);
            }
            return null;
        }

        [WebMethod(EnableSession = true)]
        public bool saveUser(User user)
        {
            if (isLoggedIn())
            {
                return Users.SaveUser(user);
            }
            return false;
        }

        [WebMethod(EnableSession = true)]
        public bool deleteUser(User user)
        {
            if (isLoggedIn())
            {
                return Users.DeleteUser(user);
            }
            return false;
        }

        [WebMethod(EnableSession = true)]
        public bool userExists(string user)
        {
            if (isLoggedIn())
            {
                return Users.UserExists(user);
            }
            return false;
        }

        [WebMethod(EnableSession = true)]
        public List<User> getAllUsers()
        {
            if (isLoggedIn())
            {
                return Users.GetAll();
            }
            return null;
        }
        #endregion

        #region Filmes
        [WebMethod(EnableSession = true)]
        public Filme getFilme(string id)
        {
            if (isLoggedIn())
            {
                return Filmes.getFilme(id);
            }
            return null;
        }

        [WebMethod(EnableSession = true)]
        public bool filmeExists(string id)
        {
            if (isLoggedIn())
            {
                return Filmes.FilmeExists(id);
            }
            return false;
        }

        [WebMethod(EnableSession = true)]
        public string saveFilme(Filme filme)
        {
            if (isLoggedIn())
            {
                if (Filmes.SaveFilme(filme))
                {
                    return filme.ID;
                }
                return null;
            }
            return null;
        }

        [WebMethod(EnableSession = true)]
        public bool deleteFilme(Filme filme)
        {
            if (isLoggedIn())
            {
                return Filmes.DeleteFilme(filme);
            }
            return false;
        }

        #region Estudios

        [WebMethod(EnableSession = true)]
        public List<Estudio> getEstudiosFromFilme(string id)
        {
            if (isLoggedIn())
            {
                return Filmes.getEstudios(id);
            }
            return null;
        }

        [WebMethod(EnableSession = true)]
        public bool addEstudio(Filme filme, Estudio estudio)
        {
            if (isLoggedIn())
            {
                return Filmes.addEstudio(filme, estudio);
            }
            return false;
        }

        [WebMethod(EnableSession = true)]
        public bool removeEstudio(Filme filme, Estudio estudio)
        {
            if (isLoggedIn())
            {
                return Filmes.removeEstudio(filme, estudio);
            }
            return false;
        }

        #endregion

        #region Produtor

        [WebMethod(EnableSession = true)]
        public List<Produtor> getProdutoresFromFilme(string id)
        {
            if (isLoggedIn())
            {
                return Filmes.getProdutores(id);
            }
            return null;
        }

        [WebMethod(EnableSession = true)]
        public bool addProdutor(Filme filme, Produtor produtor)
        {
            if (isLoggedIn())
            {
                return Filmes.addProdutor(filme, produtor);
            }
            return false;
        }

        [WebMethod(EnableSession = true)]
        public bool removeProdutor(Filme filme, Produtor produtor)
        {
            if(isLoggedIn())
            {
                return Filmes.removeProdutor(filme, produtor);
            }
            return false;
        }

        #endregion

        #region Realizador

        [WebMethod(EnableSession = true)]
        public List<Realizador> getRealizadoresFromFilme(string id)
        {
            if (isLoggedIn())
            {
                return Filmes.getRealizadores(id);
            }
            return null;
        }

        [WebMethod(EnableSession = true)]
        public bool addRealizador(Filme filme, Realizador realizador)
        {
            if (isLoggedIn())
            {
                return Filmes.addRealizador(filme, realizador);
            }
            return false;
        }

        [WebMethod(EnableSession = true)]
        public bool removeRealizador(Filme filme, Realizador realizador)
        {
            if(isLoggedIn())
            {
                return Filmes.removeRealizador(filme, realizador);
            }
            return false;
        }


        #endregion

        #region Ator

        [WebMethod(EnableSession = true)]
        public List<Ator> getAtoresFromFilme(string id)
        {
            if (isLoggedIn())
            {
                return Filmes.getAtores(id);
            }
            return null;
        }

        [WebMethod(EnableSession = true)]
        public bool addAtor(Filme filme, Ator ator)
        {
            if(isLoggedIn())
            {
                return Filmes.addAtor(filme, ator);
            }
            return false;
        }

        [WebMethod(EnableSession = true)]
        public bool removeAtor(Filme filme, Ator ator)
        {
            if(isLoggedIn())
            {
                return Filmes.removeAtor(filme, ator);
            }
            return false;
        }

        #endregion


        [WebMethod(EnableSession = true)]
        public List<Comentario> getComentariosFromFilme(string id)
        {
            if (isLoggedIn())
            {
                return Filmes.getComentarios(id);
            }
            return null;
        }


        [WebMethod(EnableSession = true)]
        public List<Filme> getFilmes()
        {
            if (isLoggedIn())
            {
                return Filmes.All();
            }
            return null;
        }

        [WebMethod(EnableSession = true)]
        public Filme getMovie(string id)
        {
            if (isLoggedIn())
            {
                return Filmes.getFilme(id);
            }
            return null;
        }

        [WebMethod(EnableSession = true)]
        public List<Filme> FilteredList(List<KeyValuePair<string,object>> keyValuePairs)
        {
            if(isLoggedIn())
            {
                return Filmes.FilteredList(keyValuePairs);
            }
            return null;
        }


        [WebMethod(EnableSession = true)]
        public bool addComentario(string filme_id, string user_id, string comentario)
        {
            if (isLoggedIn())
            {
                return Filmes.addComentario(filme_id, user_id, comentario);
            }
            return false;
        }

        [WebMethod(EnableSession = true)]
        public bool removeComentario(string id)
        {
            if (isLoggedIn())
            {
                return Filmes.removeComentario(id);
            }
            return false;
        }

        [WebMethod(EnableSession = true)]
        public bool addAvaliacao(string filme_id, string user_id, int avaliacao)
        {
            if (isLoggedIn())
            {
                return Filmes.addAvaliacao(filme_id, user_id, avaliacao);
            }
            return false;
        }

        [WebMethod(EnableSession = true)]
        public int? getAvaliacao(string filme_id, string user_id)
        {
            if (isLoggedIn())
            {
                return Filmes.getAvaliacao(filme_id, user_id);
            }
            return null;
        }

        [WebMethod(EnableSession = true)]
        public double? getAvaliacaoAvg(string filme_id)
        {
            if (isLoggedIn())
            {
                return Filmes.getAvaliacaoAvg(filme_id);
            }
            return null;
        }

        #endregion

        #region Generos
        [WebMethod(EnableSession = true)]
        public Genero getGenero(string id)
        {
            if (isLoggedIn())
            {
                return Generos.GetGenero(id);
            }
            return null;
        }

        [WebMethod(EnableSession = true)]
        public bool generoExists(string id)
        {
            if (isLoggedIn())
            {
                return Generos.GeneroExists(id);
            }
            return false;
        }

        [WebMethod(EnableSession = true)]
        public string saveGenero(Genero genero)
        {
            if (isLoggedIn())
            {
                if(Generos.SaveGenero(ref genero))
                {
                    return genero.ID;
                }
                return null;
            }
            return null;
        }

        [WebMethod(EnableSession = true)]
        public bool deleteGenero(Genero genero)
        {
            if (isLoggedIn())
            {
                return Generos.DeleteGenero(genero);
            }
            return false;
        }

        [WebMethod(EnableSession = true)]
        public List<Genero> getAllGeneros()
        {
            if (isLoggedIn())
            {
                return Generos.GetAllGeneros();
            }
            return null;
        }
        #endregion

        #region Produtores
        [WebMethod(EnableSession = true)]
        public Produtor getProdutor(string id)
        {
            if (isLoggedIn())
            {
                return Produtores.GetProdutor(id);
            }
            return null;
        }

        [WebMethod(EnableSession = true)]
        public bool produtorExists(string id)
        {
            if (isLoggedIn())
            {
                return Produtores.ProdutorExists(id);
            }
            return false;
        }

        [WebMethod(EnableSession = true)]
        public string saveProdutor(Produtor produtor)
        {
            if (isLoggedIn())
            {
                if (Produtores.SaveProdutor(ref produtor))
                {
                    return produtor.ID;
                }
                return null;
            }
            return null;
        }

        [WebMethod(EnableSession = true)]
        public bool deleteProdutor(Produtor produtor)
        {
            if (isLoggedIn())
            {
                return Produtores.DeleteProdutor(produtor);
            }
            return false;
        }

        [WebMethod(EnableSession = true)]
        public List<Produtor> getAllProdutores()
        {
            if (isLoggedIn())
            {
                return Produtores.GetAllProdutores();
            }
            return null;
        }
        #endregion

        #region Realizadores
        [WebMethod(EnableSession = true)]
        public Realizador getRealizador(string id)
        {
            if (isLoggedIn())
            {
                return Realizadores.GetRealizador(id);
            }
            return null;
        }

        [WebMethod(EnableSession = true)]
        public bool realizadorExists(string id)
        {
            if (isLoggedIn())
            {
                return Realizadores.RealizadorExists(id);
            }
            return false;
        }

        [WebMethod(EnableSession = true)]
        public string saveRealizador(Realizador realizador)
        {
            if (isLoggedIn())
            {
                if (Realizadores.SaveRealizador(ref realizador))
                {
                    return realizador.ID;
                }
                return null;
            }
            return null;
        }

        [WebMethod(EnableSession = true)]
        public bool deleteRealizador(Realizador realizador)
        {
            if (isLoggedIn())
            {
                return Realizadores.DeleteRealizador(realizador);
            }
            return false;
        }

        [WebMethod(EnableSession = true)]
        public List<Realizador> getAllRealizadores()
        {
            if (isLoggedIn())
            {
                return Realizadores.GetAllRealizadores();
            }
            return null;
        }
        #endregion

        #region Estudios
        [WebMethod(EnableSession = true)]
        public Estudio getEstudio(string id)
        {
            if (isLoggedIn())
            {
                return Estudios.GetEstudio(id);
            }
            return null;
        }

        [WebMethod(EnableSession = true)]
        public bool estudioExists(string id)
        {
            if (isLoggedIn())
            {
                return Estudios.EstudioExists(id);
            }
            return false;
        }

        [WebMethod(EnableSession = true)]
        public string saveEstudio(Estudio estudio)
        {
            if (isLoggedIn())
            {
                if (Estudios.SaveEstudio(ref estudio))
                {
                    return estudio.ID;
                }
                return null;
            }
            return null;
        }

        [WebMethod(EnableSession = true)]
        public bool deleteEstudio(Estudio estudio)
        {
            if (isLoggedIn())
            {
                return Estudios.DeleteEstudio(estudio);
            }
            return false;
        }

        [WebMethod(EnableSession = true)]
        public List<Estudio> getAllEstudios()
        {
            if (isLoggedIn())
            {
                return Estudios.GetAllEstudios();
            }
            return null;
        }



        #endregion

        #region Atores
        [WebMethod(EnableSession = true)]
        public Ator getAtor(string id)
        {
            if (isLoggedIn())
            {
                return Atores.GetAtor(id);
            }
            return null;
        }

        [WebMethod(EnableSession = true)]
        public bool atorExists(string id)
        {
            if (isLoggedIn())
            {
                return Atores.AtorExists(id);
            }
            return false;
        }

        [WebMethod(EnableSession = true)]
        public string saveAtor(Ator ator)
        {
            if (isLoggedIn())
            {
                if (Atores.SaveAtor(ref ator))
                {
                    return ator.ID;
                }
                return null;
            }
            return null;
        }

        [WebMethod(EnableSession = true)]
        public bool deleteAtor(Ator ator)
        {
            if (isLoggedIn())
            {
                return Atores.DeleteAtor(ator);
            }
            return false;
        }

        [WebMethod(EnableSession = true)]
        public List<Ator> getAllAtores()
        {
            if (isLoggedIn())
            {
                return Atores.GetAllAtores();
            }
            return null;
        }

        #endregion
    }
}
