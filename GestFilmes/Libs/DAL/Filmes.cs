﻿using GestFilmes.Libs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;


namespace GestFilmes.DAL
{
    internal static class Filmes
    {
        private static DAL dal = new DAL("Filme", Properties.Settings.Default.DBPath);

        internal static List<Filme> All()
        {
            List<Filme> filmes = new List<Filme>();

            DataTable result = dal.get($"id,titulo,genero_id,resumo,image");
            foreach (DataRow row in result.Rows)
            {
                filmes.Add(new Filme(row));
            }

            return filmes;
        }

        internal static Filme getFilme(string id)
        {
            Filme filme = null;
            DataTable result = dal.find($"id,titulo,genero_id,resumo,image", $"where id = '{SQLHelper.EscapeSQL(id)}'");
            if (result.Rows.Count > 0)
            {
                filme = new Filme(result.Rows[0]);
            }
            return filme;
        }

        internal static bool FilmeExists(string id)
        {
            return dal.exists($"where id = '{SQLHelper.EscapeSQL(id)}'");
        }

        internal static bool SaveFilme(Filme filme)
        {
            List<KeyValuePair<string, object>> kv = new List<KeyValuePair<string, object>>();
            kv.Add(new KeyValuePair<string, object>("genero_id", filme.Genero.ID));
            kv.Add(new KeyValuePair<string, object>("image", filme.Image));
            kv.Add(new KeyValuePair<string, object>("resumo", filme.Resumo));
            kv.Add(new KeyValuePair<string, object>("titulo", filme.Titulo));
            bool status = true;
            try
            {
                if (!FilmeExists(filme.ID))
                {
                    string id;
                    do
                    {
                        id = Guid.NewGuid().ToString();
                    } while (FilmeExists(id));

                    kv.Add(new KeyValuePair<string, object>("id", id));
                    status = dal.insert(kv);
                    if (status)
                        filme.ID = id;
                }
                else
                {
                    status = dal.update(kv, $"where id='{SQLHelper.EscapeSQL(filme.ID)}'");
                }
            }
            catch
            {
                status = false;
            }
            return status;
        }

        internal static bool DeleteFilme(Filme filme)
        {
            return dal.delete($"where id='{SQLHelper.EscapeSQL(filme.ID)}'");
        }

        #region Ator
        internal static List<Ator> getAtores(string id)
        {
            DAL dalAtor = new DAL("Filme_Ator", Properties.Settings.Default.DBPath);
            DataTable result = dalAtor.find("ator_id", $"where filme_id = '{SQLHelper.EscapeSQL(id)}'");

            List<Ator> atores = new List<Ator>();


            foreach (DataRow row in result.Rows)
            {
                Ator ator = Atores.GetAtor(row["ator_id"].ToString());
                if (ator != null)
                {
                    atores.Add(ator);
                }
            }

            return atores;
        }

        internal static bool addAtor(Filme filme, Ator ator)
        {
            DAL dalAtor = new DAL("Filme_Ator", Properties.Settings.Default.DBPath);
            if (!dalAtor.exists($"where filme_id='{SQLHelper.EscapeSQL(filme.ID)}' and ator_id='{SQLHelper.EscapeSQL(ator.ID)}'"))
            {
                List<KeyValuePair<string, object>> kv = new List<KeyValuePair<string, object>>();
                kv.Add(new KeyValuePair<string, object>("filme_id", filme.ID));
                kv.Add(new KeyValuePair<string, object>("ator_id", ator.ID));
                return dalAtor.insert(kv);
            }
            return false;
        }

        internal static bool removeAtor(Filme filme, Ator ator)
        {
            DAL dalAtor = new DAL("Filme_Ator", Properties.Settings.Default.DBPath);
            if (dalAtor.exists($"where filme_id='{SQLHelper.EscapeSQL(filme.ID)}' and ator_id='{SQLHelper.EscapeSQL(ator.ID)}'"))
            {
                return dalAtor.delete($"where filme_id='{SQLHelper.EscapeSQL(filme.ID)}' and ator_id='{SQLHelper.EscapeSQL(ator.ID)}'");
            }
            return false;
        }

        #endregion

        #region Estudio

        internal static List<Estudio> getEstudios(string id)
        {
            DAL dalEstudio = new DAL("Filme_Estudio", Properties.Settings.Default.DBPath);
            DataTable result = dalEstudio.find("estudio_id", $"where filme_id = '{SQLHelper.EscapeSQL(id)}'");

            List<Estudio> estudios = new List<Estudio>();


            foreach (DataRow row in result.Rows)
            {
                Estudio estudio = Estudios.GetEstudio(row["estudio_id"].ToString());
                if (estudio != null)
                {
                    estudios.Add(estudio);
                }
            }

            return estudios;
        }

        internal static bool addEstudio(Filme filme, Estudio estudio)
        {
            DAL dalEstudio = new DAL("Filme_Estudio", Properties.Settings.Default.DBPath);
            if (!dalEstudio.exists($"where filme_id='{SQLHelper.EscapeSQL(filme.ID)}' and estudio_id='{SQLHelper.EscapeSQL(estudio.ID)}'"))
            {
                List<KeyValuePair<string, object>> kv = new List<KeyValuePair<string, object>>();
                kv.Add(new KeyValuePair<string, object>("filme_id", filme.ID));
                kv.Add(new KeyValuePair<string, object>("estudio_id", estudio.ID));
                return dalEstudio.insert(kv);
            }
            return false;
        }

        internal static bool removeEstudio(Filme filme, Estudio estudio)
        {
            DAL dalEstudio = new DAL("Filme_Estudio", Properties.Settings.Default.DBPath);
            if (dalEstudio.exists($"where filme_id='{SQLHelper.EscapeSQL(filme.ID)}' and estudio_id='{SQLHelper.EscapeSQL(estudio.ID)}'"))
            {
                return dalEstudio.delete($"where filme_id='{SQLHelper.EscapeSQL(filme.ID)}' and estudio_id='{SQLHelper.EscapeSQL(estudio.ID)}'");
            }
            return false;
        }
        #endregion

        #region Realizador

        internal static List<Realizador> getRealizadores(string id)
        {
            DAL dalRealizador = new DAL("Filme_Realizador", Properties.Settings.Default.DBPath);
            DataTable result = dalRealizador.find("realizador_id", $"where filme_id = '{SQLHelper.EscapeSQL(id)}'");

            List<Realizador> realizadores = new List<Realizador>();


            foreach (DataRow row in result.Rows)
            {
                Realizador realizador = Realizadores.GetRealizador(row["realizador_id"].ToString());
                if (realizador != null)
                {
                    realizadores.Add(realizador);
                }
            }

            return realizadores;
        }

        internal static bool addRealizador(Filme filme, Realizador realizador)
        {
            DAL dalRealizador = new DAL("Filme_Realizador", Properties.Settings.Default.DBPath);
            if (!dalRealizador.exists($"where filme_id='{SQLHelper.EscapeSQL(filme.ID)}' and realizador_id='{SQLHelper.EscapeSQL(realizador.ID)}'"))
            {
                List<KeyValuePair<string, object>> kv = new List<KeyValuePair<string, object>>();
                kv.Add(new KeyValuePair<string, object>("filme_id", filme.ID));
                kv.Add(new KeyValuePair<string, object>("realizador_id", realizador.ID));
                return dalRealizador.insert(kv);
            }
            return false;
        }

        internal static bool removeRealizador(Filme filme, Realizador realizador)
        {
            DAL dalRealizador = new DAL("Filme_Realizador", Properties.Settings.Default.DBPath);
            if (dalRealizador.exists($"where filme_id='{SQLHelper.EscapeSQL(filme.ID)}' and realizador_id='{SQLHelper.EscapeSQL(realizador.ID)}'"))
            {
                return dalRealizador.delete($"where filme_id='{SQLHelper.EscapeSQL(filme.ID)}' and realizador_id='{SQLHelper.EscapeSQL(realizador.ID)}'");
            }
            return false;
        }

        #endregion

        #region Produtor

        internal static List<Produtor> getProdutores(string id)
        {
            DAL dalProdutor = new DAL("Filme_Produtor", Properties.Settings.Default.DBPath);
            DataTable result = dalProdutor.find("produtor_id", $"where filme_id = '{SQLHelper.EscapeSQL(id)}'");

            List<Produtor> produtores = new List<Produtor>();


            foreach (DataRow row in result.Rows)
            {
                Produtor produtor = Produtores.GetProdutor(row["produtor_id"].ToString());
                if (produtor != null)
                {
                    produtores.Add(produtor);
                }
            }

            return produtores;
        }

        internal static bool addProdutor(Filme filme, Produtor produtor)
        {
            DAL dalProdutor = new DAL("Filme_Produtor", Properties.Settings.Default.DBPath);
            if (!dalProdutor.exists($"where filme_id='{SQLHelper.EscapeSQL(filme.ID)}' and produtor_id='{SQLHelper.EscapeSQL(produtor.ID)}'"))
            {
                List<KeyValuePair<string, object>> kv = new List<KeyValuePair<string, object>>();
                kv.Add(new KeyValuePair<string, object>("filme_id", filme.ID));
                kv.Add(new KeyValuePair<string, object>("produtor_id", produtor.ID));
                return dalProdutor.insert(kv);
            }
            return false;
        }

        internal static bool removeProdutor(Filme filme, Produtor produtor)
        {
            DAL dalProdutor = new DAL("Filme_Produtor", Properties.Settings.Default.DBPath);
            if (dalProdutor.exists($"where filme_id='{SQLHelper.EscapeSQL(filme.ID)}' and produtor_id='{SQLHelper.EscapeSQL(produtor.ID)}'"))
            {
                return dalProdutor.delete($"where filme_id='{SQLHelper.EscapeSQL(filme.ID)}' and produtor_id='{SQLHelper.EscapeSQL(produtor.ID)}'");
            }
            return false;
        }

        #endregion

        #region Comentario
        internal static List<Comentario> getComentarios(string id)
        {
            DAL dalComentarios = new DAL("Comentario", Properties.Settings.Default.DBPath);
            List<Comentario> comentarios = new List<Comentario>();
            DataTable result = dalComentarios.find($"user_id,conteudo,id,created_at", $"where filme_id = '{SQLHelper.EscapeSQL(id)}' order by created_at desc");
            foreach (DataRow row in result.Rows)
            {
                comentarios.Add(new Comentario(row));
            }
            return comentarios;
        }

        internal static bool addComentario(string filme_id, string user_id, string comentario)
        {
            DAL dalComentario = new DAL("Comentario", Properties.Settings.Default.DBPath);
            List<KeyValuePair<string, object>> kv = new List<KeyValuePair<string, object>>();
            string id;
            do
            {
                id = Guid.NewGuid().ToString();
            } while (dalComentario.exists($"where id='{id}'"));
            kv.Add(new KeyValuePair<string, object>("id", id));
            kv.Add(new KeyValuePair<string, object>("filme_id", filme_id));
            kv.Add(new KeyValuePair<string, object>("user_id", user_id));
            kv.Add(new KeyValuePair<string, object>("conteudo", comentario));
            return dalComentario.insert(kv);
        }

        internal static bool removeComentario(string id)
        {
            DAL dalComentario = new DAL("Comentario", Properties.Settings.Default.DBPath);
            return dalComentario.delete($"where id='{SQLHelper.EscapeSQL(id)}'");
        }

        #endregion

        #region Avaliacao

        internal static bool addAvaliacao(string filme_id, string user_id, int avaliacao)
        {
            DAL dalAvaliacao = new DAL("Avaliacao", Properties.Settings.Default.DBPath);
            if (avaliacao >= 0 && avaliacao <= 5)
            {
                List<KeyValuePair<string, object>> kv = new List<KeyValuePair<string, object>>();
                kv.Add(new KeyValuePair<string, object>("avaliacao", avaliacao));
                if (dalAvaliacao.exists($"where filme_id='{filme_id}' and user_id='{user_id}'"))
                {
                    if (avaliacao == 0)
                    {
                        return dalAvaliacao.delete($"where filme_id={filme_id} and user_id='{user_id}'");
                    }
                    else
                    {
                        return dalAvaliacao.update(kv, $"where filme_id='{filme_id}' and user_id='{user_id}'");
                    }
                }
                else
                {
                    if (avaliacao != 0)
                    {
                        kv.Add(new KeyValuePair<string, object>("filme_id", filme_id));
                        kv.Add(new KeyValuePair<string, object>("user_id", user_id));
                        return dalAvaliacao.insert(kv);
                    }
                }
            }
            return false;
        }

        internal static int? getAvaliacao(string filme_id, string user_id)
        {
            DAL dalAvaliacao = new DAL("Avaliacao", Properties.Settings.Default.DBPath);
            DataTable result = dalAvaliacao.find($"avaliacao", $"where filme_id = '{SQLHelper.EscapeSQL(filme_id)}' and user_id='{SQLHelper.EscapeSQL(user_id)}'");
            if (result.Rows.Count > 0)
            {
                return result.Rows[0]["avaliacao"] as int?;
            }
            return null;
        }

        internal static double? getAvaliacaoAvg(string filme_id)
        {
            DAL dalAvaliacao = new DAL("Avaliacao", Properties.Settings.Default.DBPath);
            DataTable result = dalAvaliacao.find($"avg(avaliacao) as averageRating", $"where filme_id = '{SQLHelper.EscapeSQL(filme_id)}'");
            if (result.Rows.Count > 0)
            {
                string debug = result.Rows[0][0] as string;
                return result.Rows[0]["averageRating"] as double?;
            }
            return null;
        }

        #endregion

        internal static List<Filme> FilteredList(List<KeyValuePair<string, object>> keyValuePairs)
        {
            if (keyValuePairs.Count == 0)
                return All();

            List<string> IDs = null;

            foreach (KeyValuePair<string, object> keyValuePair in keyValuePairs)
            {
                switch (keyValuePair.Key)
                {
                    case "Ator":
                        IDs = MergeLists(IDs, Filmes.SearchAtor(keyValuePair.Value));
                        break;
                    case "Realizador":
                        IDs = MergeLists(IDs,Filmes.SearchRealizador(keyValuePair.Value));
                        break;
                    case "Estudio":
                        IDs = MergeLists(IDs, Filmes.SearchEstudio(keyValuePair.Value));
                        break;
                    case "Produtor":
                        IDs = MergeLists(IDs, Filmes.SearchProdutor(keyValuePair.Value));
                        break;
                    case "Genero":
                        IDs = MergeLists(IDs, Filmes.SearchGenero(keyValuePair.Value));
                        break;
                    case "Filme":
                        IDs = MergeLists(IDs, Filmes.SearchTitulo(keyValuePair.Value as string));
                        break;
                    default:
                        //useless
                        break;
                }
            }

            if(IDs == null)
                return All();

            List<Filme> filmes = new List<Filme>();
            foreach (string id in IDs)
            {
                Filme temp = getFilme(id);
                if(temp!=null)
                    filmes.Add(temp);
            }

            return filmes;
        }

        private static List<string> MergeLists(List<string> IDs, List<string> list)
        {
            if (IDs == null)
            {
                return list;
            }
            else
            {
                return IDs.Intersect(list).ToList();
            }
        }

        private static List<string> SearchTitulo(string value)
        {
            List<string> IDs = new List<string>();
            DataTable result = dal.DirectQuery($"select distinct [id] from [Filme] where [titulo] like '%{value}%'");
            foreach (DataRow row in result.Rows)
            {
                IDs.Add(row["id"] as string);
            }
            return IDs;
        }

        private static List<string> SearchGenero(object value)
        {
            List<string> IDs = new List<string>();
            DataTable result;
            if (value.GetType() == typeof(int))
            {
                result = dal.DirectQuery($"select distinct [id] from [Filme] where [genero_id] = {value}");
            }
            else
            {
                result = dal.DirectQuery($"select distinct [id] from [Filme] where [genero_id] in ( select [id] from [Genero] where [nome] like '%{value}%' )");
            }
            foreach (DataRow row in result.Rows)
            {
                IDs.Add(row["id"] as string);
            }
            return IDs;
        }

        private static List<string> SearchProdutor(object value)
        {
            List<string> IDs = new List<string>();
            DataTable result;
            if (value.GetType() == typeof(int))
            {
                result = dal.DirectQuery($"select distinct [filme_id] from [Filme_Produtor] where [produtor_id] = {value}");
            }
            else
            {
                result = dal.DirectQuery($"select distinct [filme_id] from [Filme_Produtor] where [produtor_id] in ( select [id] from [Produtor] where [nome] like '%{value}%' )");
            }
            foreach (DataRow row in result.Rows)
            {
                IDs.Add(row["filme_id"] as string);
            }
            return IDs;
        }

        private static List<string> SearchEstudio(object value)
        {
            List<string> IDs = new List<string>();
            DataTable result;
            if (value.GetType() == typeof(int))
            {
                result = dal.DirectQuery($"select distinct [filme_id] from [Filme_Estudio] where [estudio_id] = {value}");
            }
            else
            {
                result = dal.DirectQuery($"select distinct [filme_id] from [Filme_Estudio] where [estudio_id] in ( select [id] from [Estudio] where [nome] like '%{value}%' )");
            }
            foreach (DataRow row in result.Rows) {
                IDs.Add(row["filme_id"] as string);
            }
            return IDs;
        }

        private static List<string> SearchRealizador(object value)
        {
            List<string> IDs = new List<string>();
            DataTable result;
            if (value.GetType() == typeof(int))
            {
                result = dal.DirectQuery($"select distinct [filme_id] from [Filme_Realizador] where [realizador_id] = {value}");
            }
            else
            {
                result = dal.DirectQuery($"select distinct [filme_id] from [Filme_Realizador] where [realizador_id] in ( select [id] from [Realizador] where [nome] like '%{value}%' )");
            }
            foreach (DataRow row in result.Rows)
            {
                IDs.Add(row["filme_id"] as string);
            }
            return IDs;
        }

        private static List<string> SearchAtor(object value)
        {
            List<string> IDs = new List<string>();
            DataTable result;
            if (value.GetType() == typeof(int))
            {
                result = dal.DirectQuery($"select distinct [filme_id] from [Filme_Ator] where [ator_id] = {value}");
            }
            else
            {
                result = dal.DirectQuery($"select distinct [filme_id] from [Filme_Ator] where [ator_id] in ( select [id] from [Ator] where [nome] like '%{value}%' )");
            }
            foreach (DataRow row in result.Rows)
            {
                IDs.Add(row["filme_id"] as string);
            }
            return IDs;
        }
    }
}