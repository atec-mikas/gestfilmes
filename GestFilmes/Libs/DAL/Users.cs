﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using GestFilmes.Libs;

namespace GestFilmes.DAL
{
    internal static class Users
    {
        private static DAL dal = new DAL("Utilizador", Properties.Settings.Default.DBPath);
    
        internal static User GetUser(string username)
        {
            User user = null;
            DataTable result = dal.find($"username,password,tipo", $"where username = '{SQLHelper.EscapeSQL(username)}'");
            if (result.Rows.Count>0)
            {
                user = new User(result.Rows[0]);
            }
            return user;
        }

        internal static List<User> GetAll()
        {
            List<User> users = new List<User>();
            DataTable result = dal.get("username,password,tipo");
            foreach(DataRow row in result.Rows)
            {
                users.Add(new User(row));
            }
            return users;
        }

        internal static bool UserExists(string username)
        {
            return dal.exists($"where username = '{SQLHelper.EscapeSQL(username)}'");
        }

        internal static bool SaveUser(User user)
        {
            List<KeyValuePair<string, object>> kv = new List<KeyValuePair<string, object>>();
            kv.Add(new KeyValuePair<string, object>("password", user.Password));
            kv.Add(new KeyValuePair<string, object>("tipo", (int)user.TipoUser));
            bool status = true;
            try
            {
                if(!UserExists(user.Username))
                {
                    kv.Add(new KeyValuePair<string, object>("username", user.Username));
                    status = dal.insert(kv);
                }
                else
                {
                    status = dal.update(kv,$"where username='{SQLHelper.EscapeSQL(user.Username)}'");
                }
            }
#pragma warning disable CS0168 // Variable is declared but never used
            catch (Exception e)
#pragma warning restore CS0168 // Variable is declared but never used
            {
                status = false;
            }
            return status;
        }

        internal static bool DeleteUser(User user)
        {
            return dal.delete($"where username='{SQLHelper.EscapeSQL(user.Username)}'");
        }
    }
}