﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Title="Mostrar Filme" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="GestFilmes.Pages.FrontOffice.Movies.WebForm1" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">

    <div class="container">
        <div class="row mx-auto w-100 mt-3">
            <div runat="server" id="linkEditMovie">
            </div>
            <div runat="server" id="linkEraseMovie">
                <asp:LinkButton runat="server" CssClass="ml-3 btn btn-outline-danger" Text="Eliminar filme" OnClick="btnDeleteMovie"></asp:LinkButton>
            </div>
        </div>
        <hr />
        <div id="mvi" runat="server" class="filmes" style="margin-bottom:20px;">
        </div>
        <div class="row">
            <div class="col-md-12">
                <div>
                    <div class="float-left">
                        <h3>⭐ Avaliação</h3>
                    </div>
                    <div class="float-right">
                        <div id="ratingResult" runat="server"></div>
                    </div>
                </div>
                    <%if (Session["logIn"] != null)
                        {        %>
                <div id="Div1" runat="server">
                    <div class="form-group">
                        <asp:TextBox TextMode="Number" CssClass="form-control" runat="server" ID="txtRating" min="0" max="5"></asp:TextBox>
                    </div>
                    <asp:Button ID="btnSaveRating" runat="server" CssClass="btn btn-outline-secondary" Text="Avaliar Filme" OnClick="btnSaveRating_Click" />
                </div>
                <%} %>
            </div>
        </div>
        <hr />
        <div id="generoDoFilme" runat="server" class="generoDoFilme">
        </div>
        <div id="estudiosdoFilme" runat="server" class="estudiosDoFilme">
        </div>
        <div id="atoresDoFilme" runat="server" class="atoresDoFilme">
        </div>
        <div id="produtoresDoFilme" runat="server" class="produtoresDoFilme">
        </div>
        <div id="realizadoresDoFilme" runat="server" class="realizadoresDoFilme">
        </div>
    </div>

    <%if (Session["logIn"] != null)
        {        %>

    <hr />
    <div class="row">
        <div class="col-md-12">
            <h3>💬 Comentar</h3>
            <hr />
            <div id="formComentario" runat="server">
                <div class="form-group">
                    <asp:TextBox runat="server" ID="txtComentario" CssClass="form-control"></asp:TextBox>
                </div>
                <asp:Button runat="server" ID="btnAddComment" OnClick="btnAddComment_Click" CssClass="btn btn-primary" Text="Comentar" />
            </div>
            <hr />
            <h2>Comentários:</h2>
            <div id="comentariosDoFilme" runat="server">
            </div>
        </div>

    </div>
    <% }%>
</asp:Content>
