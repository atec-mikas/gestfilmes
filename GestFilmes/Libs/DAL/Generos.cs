﻿using GestFilmes.Libs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace GestFilmes.DAL
{
    internal static class Generos
    {
        private static DAL dal = new DAL("Genero", Properties.Settings.Default.DBPath);

        internal static Genero GetGenero(string id)
        {
            Genero genero = null;
            DataTable result = dal.find($"id,nome", $"where id = '{SQLHelper.EscapeSQL(id)}'");
            if (result.Rows.Count > 0)
            {
                genero = new Genero(result.Rows[0]);
            }
            return genero;
        }

        internal static bool GeneroExists(string id)
        {
            return dal.exists($"where id = '{SQLHelper.EscapeSQL(id)}'");
        }

        internal static bool SaveGenero(ref Genero genero)
        {
            List<KeyValuePair<string, object>> kv = new List<KeyValuePair<string, object>>();
            kv.Add(new KeyValuePair<string, object>("nome", genero.Nome));
            bool status = true;
            try
            {
                if (!GeneroExists(genero.ID))
                {
                    string id;
                    do
                    {
                        id = Guid.NewGuid().ToString();
                    } while (GeneroExists(id));

                    kv.Add(new KeyValuePair<string, object>("id", id));
                    status = dal.insert(kv);
                    if (status)
                        genero.ID = id;
                }
                else
                {
                    status = dal.update(kv, $"where id='{SQLHelper.EscapeSQL(genero.ID)}'");
                }
            }
            catch
            {
                status = false;
            }
            return status;
        }

        internal static bool DeleteGenero(Genero genero)
        {
            DAL dalFilme = new DAL("Filme", Properties.Settings.Default.DBPath);
            return dalFilme.delete($"where genero_id='{SQLHelper.EscapeSQL(genero.ID)}'")
                    && dal.delete($"where id='{SQLHelper.EscapeSQL(genero.ID)}'");
        }

        internal static List<Genero> GetAllGeneros()
        {
            List<Genero> generos = new List<Genero>();
            DataTable result = dal.get($"id,nome");
            if (result.Rows.Count > 0)
            {
                foreach (DataRow genero in result.Rows)
                    generos.Add(new Genero(genero));
            }
            return generos;
        }
    }
}