﻿using GestFilmes.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestFilmes.Libs
{
    public static class WebHelper
    {
        public static bool isLoggedIn(User user, TipoUser tipoUser = TipoUser.Registado)
        {
            Access webAPI = WebHelper.CreateWebAPI();
            return user != null && webAPI.userExists(user.Username) && user.TipoUser >= tipoUser;
        }

        public static Access CreateWebAPI()
        {
            Access webAPI = new Access();
            webAPI.logIn(Properties.Settings.Default.AuthKey);
            return webAPI;
        }

    }
}