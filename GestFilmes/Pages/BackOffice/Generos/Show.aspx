﻿<%@ Page Language="C#" MasterPageFile="~/BackOffice.Master" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="GestFilmes.Pages.BackOffice.Generos.WebForm1" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <div ID="genres" runat="server" class="genres">
        <div class="container mt-5">
            <div class="card">
                <div class="card-body">
                    <div ID="nomeGenero" runat="server" class="nomeGenero">  
                    </div>
                    <div ID="Div1" runat="server" class="nomeGenero">
                        <div class="float-right">
                            <asp:Button runat="server" OnClick="deleteGenero" CssClass="btn btn-outline-danger" id="deleteGeneroButton" Text="Apagar genero"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>