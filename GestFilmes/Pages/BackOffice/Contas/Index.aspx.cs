﻿using GestFilmes.DAL;
using GestFilmes.Libs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GestFilmes.Pages.BackOffice.Contas
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Access WebApi = WebHelper.CreateWebAPI();
            User usr = Session["logIn"] as User;
            if (Session["logIn"] == null)
            {
                Response.Redirect("~/Pages/Account/Login.aspx");
            }
            if (!WebHelper.isLoggedIn(usr))
            {
                Response.Redirect("~/Pages/FrontOffice/Dashboard.aspx");
            }
            var contas = WebApi.getAllUsers();


            Dcontas.InnerHtml = "<ul>";
            foreach (var item in contas)
            {
                Dcontas.InnerHtml += "<li><a href='Show.aspx?id=" + item.Username + "'>" + item.Username + "</a></li><hr/>";
            }
            Dcontas.InnerHtml += "</ul>";
        }
    }
}