﻿using GestFilmes.Libs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace GestFilmes.DAL
{
    internal static class Produtores
    {
        private static DAL dal = new DAL("Produtor", Properties.Settings.Default.DBPath);

        internal static Produtor GetProdutor(string id)
        {
            Produtor produtor = null;
            DataTable result = dal.find($"id,nome", $"where id = '{SQLHelper.EscapeSQL(id)}'");
            if (result.Rows.Count > 0)
            {
                produtor = new Produtor(result.Rows[0]);
            }
            return produtor;
        }

        internal static List<Produtor> GetAllProdutores()
        {
            List<Produtor> produtores = new List<Produtor>();
            DataTable result = dal.get($"id,nome");
            if (result.Rows.Count > 0)
            {
                foreach (DataRow produtor in result.Rows)
                    produtores.Add(new Produtor(produtor));
            }
            return produtores;
        }

        internal static bool ProdutorExists(string id)
        {
            return dal.exists($"where id = '{SQLHelper.EscapeSQL(id)}'");
        }

        internal static bool SaveProdutor(ref Produtor produtor)
        {
            List<KeyValuePair<string, object>> kv = new List<KeyValuePair<string, object>>();
            kv.Add(new KeyValuePair<string, object>("nome", produtor.Nome));
            bool status = true;
            try
            {
                if (!ProdutorExists(produtor.ID))
                {
                    string id;
                    do
                    {
                        id = Guid.NewGuid().ToString();
                    } while (ProdutorExists(id));

                    kv.Add(new KeyValuePair<string, object>("id", id));
                    status = dal.insert(kv);
                    if (status)
                        produtor.ID = id;
                }
                else
                {
                    status = dal.update(kv, $"where id='{SQLHelper.EscapeSQL(produtor.ID)}'");
                }
            }
            catch
            {
                status = false;
            }
            return status;
        }

        internal static bool DeleteProdutor(Produtor produtor)
        {
            DAL dalFilmeProdutor = new DAL("Filme_Produtor", Properties.Settings.Default.DBPath);
            return dalFilmeProdutor.delete($"where produtor_id='{SQLHelper.EscapeSQL(produtor.ID)}'")
                    && dal.delete($"where id='{SQLHelper.EscapeSQL(produtor.ID)}'");
        }


    }
}