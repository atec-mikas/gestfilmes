﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GestFilmes.DAL;
using GestFilmes.Libs;

namespace GestFilmes.Pages.BackOffice.Produtores
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private Access WebApi;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            WebApi = WebHelper.CreateWebAPI();
           
            User usr = Session["logIn"] as User;
            if (!WebHelper.isLoggedIn(usr, TipoUser.Gestor))
            {
                Response.Redirect("~/Pages/Account/Login.aspx");
            }
            if (!WebHelper.isLoggedIn(usr))
            {
                Response.Redirect("~/Pages/FrontOffice/Dashboard.aspx");
            }
            if (Request.QueryString["id"] == null || WebApi.getProdutor(Request.QueryString["id"])==null )
            {
                Response.Redirect("~/Pages/BackOffice/Dashboard.aspx");
            }
            Produtor produtor = WebApi.getProdutor(Request.QueryString["id"]);
            nomeProdutor.InnerHtml = "<span>Produtor número: " + produtor.ID + "<br> Nome Produtor: " + produtor.Nome + "</span>"; 
        }
        protected void deleteProdutor(object sender, EventArgs e)
        {
            Produtor produtor = WebApi.getProdutor(Request.QueryString["id"]);
            if(WebApi.deleteProdutor(produtor) == true)
            {
                Response.Redirect("~/Pages/BackOffice/Produtores/Index.aspx");
                Response.Write("Produtor " + produtor.Nome + " eliminado com sucesso!");
            }else
            {
                Response.Write("Erro ao apagar produtor!");
            }

        }
    }
}