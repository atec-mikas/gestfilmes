﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestFilmes.DAL
{
    internal static class SQLHelper
    {
        /// <summary>
        /// Função para dar escape a alguns caracteres
        /// </summary>
        /// <param name="s">String a dar escape</param>
        /// <returns>String escapada</returns>
        internal static string EscapeSQL(string s)
        {
            return s.Replace("'", "''");
        }
    }
}