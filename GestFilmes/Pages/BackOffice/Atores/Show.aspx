﻿<%@ Page Language="C#" MasterPageFile="~/BackOffice.Master" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="GestFilmes.Pages.BackOffice.Atores.WebForm1" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <div ID="actors" runat="server" class="actors">
        <div class="container mt-5">
            <div class="card">
                <div class="card-body">
                    <div ID="nomeAtor" runat="server" class="nomeAtor">
                    </div>
                    <div ID="Div1" runat="server" class="nomeAtor">
                        <div class="float-right">
                            <asp:Button runat="server" OnClick="deleteAtor" CssClass="btn btn-outline-danger" id="deleteAtorButton" Text="Apagar ator"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>