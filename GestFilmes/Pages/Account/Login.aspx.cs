﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GestFilmes.DAL;
using GestFilmes.Libs;

namespace GestFilmes.Pages.Account
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["logIn"] != null)
            {
                Response.Redirect("~/Pages/FrontOffice/Dashboard.aspx");
            }
        }

        protected void LogIn(object sender, EventArgs e)
        {
            Access WebApi = WebHelper.CreateWebAPI();
            Libs.User user = WebApi.getUser(Username.Text);
            if (user != null && user.VerifyPassword(Password.Text))
            {
                Session["logIn"] = user;
                if(user.TipoUser==TipoUser.Gestor)
                {
                    Response.Redirect("~/Pages/BackOffice/Dashboard.aspx");
                } else
                {
                    Response.Redirect("~/Pages/FrontOffice/Dashboard.aspx");
                }
            }
            else
            {
                warnings.InnerHtml= "<div class='alert alert-warning' role='alert'>Username doesn't exist or password is incorrect (:), please register first!!</div>";
            }
        }
    }
}