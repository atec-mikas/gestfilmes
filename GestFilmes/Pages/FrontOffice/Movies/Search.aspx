﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="GestFilmes.Pages.FrontOffice.Movies.Search" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="row mx-auto w-100 mt-3">
            <div class="col-md-3 mx-auto">
                <h2>Pesquisar filme</h2>
            </div>
        </div>
        <hr />
        <div class="row mx-auto w-100">
            <div class="col-md-2">
                <asp:Label runat="server" Text="Nome do Filme:"></asp:Label>
                <asp:TextBox runat="server" ID="txtFilme" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-md-2">
                <asp:Label runat="server" Text="Género do Filme:"></asp:Label>
                <asp:TextBox runat="server" ID="txtGenero" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-md-2">
                <asp:Label runat="server" Text="Estúdio do Filme:"></asp:Label>
                <asp:TextBox runat="server" ID="txtEstudio" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-md-2">
                <asp:Label runat="server" Text="Produtor do Filme:"></asp:Label>
                <asp:TextBox runat="server" ID="txtProdutor" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-md-2">
                <asp:Label runat="server" Text="Realizador do Filme:"></asp:Label>
                <asp:TextBox runat="server" ID="txtRealizador" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-md-2">
                <asp:Label runat="server" Text="Ator do Filme:"></asp:Label>
                <asp:TextBox runat="server" ID="txtAtor" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="container mx-auto w-100">
            <br />
            <asp:Button runat="server" ID="btnSearch" OnClick="btnSearch_Click" CssClass="btn btn-primary" Text="Pesquisar Filme" />
        </div>
        <div runat="server" ID="resultadosPesquisa">

        </div>
        
    </div>

</asp:Content>
