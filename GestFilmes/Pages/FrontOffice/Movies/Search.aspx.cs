﻿using GestFilmes.Libs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GestFilmes.Pages.FrontOffice.Movies
{
    public partial class Search : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            resultadosPesquisa.InnerHtml = "";
            List<KeyValuePair<string, object>> keyValuePairs = new List<KeyValuePair<string, object>>();
            if(!string.IsNullOrWhiteSpace(txtAtor.Text))
            {
                keyValuePairs.Add(new KeyValuePair<string, object>("Ator", txtAtor.Text));
            }
            if(!string.IsNullOrWhiteSpace(txtRealizador.Text))
            {
                keyValuePairs.Add(new KeyValuePair<string, object>("Realizador", txtRealizador.Text));
            }
            if(!string.IsNullOrWhiteSpace(txtProdutor.Text))
            {
                keyValuePairs.Add(new KeyValuePair<string, object>("Produtor", txtProdutor.Text));
            }
            if(!string.IsNullOrWhiteSpace(txtEstudio.Text))
            {
                keyValuePairs.Add(new KeyValuePair<string, object>("Estudio", txtEstudio.Text));
            }
            if(!string.IsNullOrWhiteSpace(txtGenero.Text))
            {
                keyValuePairs.Add(new KeyValuePair<string, object>("Genero", txtGenero.Text));
            }
            if(!string.IsNullOrWhiteSpace(txtFilme.Text))
            {
                keyValuePairs.Add(new KeyValuePair<string, object>("Filme", txtFilme.Text));
            }

            var webapi = WebHelper.CreateWebAPI();
            var results = webapi.FilteredList(keyValuePairs);
            int i = 0;
            foreach(Filme result in results)
            {
                i++;
                resultadosPesquisa.InnerHtml += "<div class='mt-2 card text-center'>";
                resultadosPesquisa.InnerHtml += $"<div class='card-header'>Resultado: {i}</div>";
                resultadosPesquisa.InnerHtml +=  "<div class='card-body'>";
                resultadosPesquisa.InnerHtml += $"<h5 ID='nomeFilme' class='card-title'>{result.Titulo}</h5>";
                resultadosPesquisa.InnerHtml += $"<p ID='resumoFilme' class='card-text'>{result.Resumo}</p>";
                resultadosPesquisa.InnerHtml += $"<a class='btn btn-outline-primary' href='Show.aspx?id=" + result.ID + "'>Ver Filme<a/></div></div>";
            }
            if(results.Count() == 0)
            {
                resultadosPesquisa.InnerHtml = "<h3 class='text-center'>Sem resultados!</h3>";
            }
        }
    }
}