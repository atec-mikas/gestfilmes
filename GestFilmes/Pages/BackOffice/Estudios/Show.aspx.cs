﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GestFilmes.DAL;
using GestFilmes.Libs;

namespace GestFilmes.Pages.BackOffice.Estudios
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private Access WebApi;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            WebApi = WebHelper.CreateWebAPI();
           
            User usr = Session["logIn"] as User;
            if (!WebHelper.isLoggedIn(usr, TipoUser.Gestor))
            {
                Response.Redirect("~/Pages/Account/Login.aspx");
            }
            if (!WebHelper.isLoggedIn(usr))
            {
                Response.Redirect("~/Pages/FrontOffice/Dashboard.aspx");
            }
            if (Request.QueryString["id"] == null || WebApi.getEstudio(Request.QueryString["id"])==null)
            {
                Response.Redirect("~/Pages/BackOffice/Dashboard.aspx");
            }
            Estudio estudio = WebApi.getEstudio(Request.QueryString["id"]);
            nomeEstudio.InnerHtml = "<span>Estúdio número: " + estudio.ID + "<br> Nome estúdio: " + estudio.Nome + "</span>"; 
        }
        protected void deleteEstudio(object sender, EventArgs e)
        {
            Estudio estudio = WebApi.getEstudio(Request.QueryString["id"]);
            if(WebApi.deleteEstudio(estudio) == true)
            {
                Response.Redirect("~/Pages/BackOffice/Estudios/Index.aspx");
                Response.Write("Estúdio" + estudio.Nome + "eliminado com sucesso!");
            }else
            {
                Response.Write("Erro ao apagar estúdio");
            }

        }
    }
}