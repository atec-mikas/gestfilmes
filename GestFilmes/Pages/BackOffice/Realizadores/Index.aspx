﻿<%@ Page Title="Dashboard" Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs"
    Inherits="GestFilmes.Pages.BackOffice.Realizadores.Index" MasterPageFile="~/BackOffice.Master"%>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <div ID="linkCreateDirector" runat="server" class="mx-auto mt-4" visible="true" style="width:200px;">
        <asp:HyperLink runat='server' NavigateUrl='~/Pages/BackOffice/Realizadores/Create.aspx' CssClass='btn btn-outline-secondary'>+ Adicionar novo realizador</asp:HyperLink>
    </div>
    <hr />
    <div class="container mt-5">
         <div class="card">
             <div class="card-body">
                 <h5 class="card-title">Lista de Realizadores</h5>
                 <div ID="realizadores" runat="server" class="realizadores">
                 </div>
             </div>
         </div>        
    </div>
</asp:Content>
