﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GestFilmes.DAL;
using GestFilmes.Libs;

namespace GestFilmes.Pages.BackOffice.Realizadores
{
    public partial class Create : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Access WebApi = WebHelper.CreateWebAPI();
            User usr = Session["logIn"] as User;
            if (Session["logIn"] == null)
            {
                Response.Redirect("~/Pages/Account/Login.aspx");
            }
            if (!WebHelper.isLoggedIn(usr))
            {
                Response.Redirect("~/Pages/FrontOffice/Dashboard.aspx");
            }
        }

        protected void addDirector(object sender, EventArgs e)
        {
            Access WebApi = WebHelper.CreateWebAPI();
            if (Name.Text != null)
            {
                Realizador realizador = new Realizador();
                realizador.Nome = Name.Text;
                if (WebApi.saveRealizador(realizador) != null)
                {
                    Response.Redirect("~/Pages/BackOffice/Realizadores/Index.aspx");
                }
                else
                {
                    Response.Write("Error saving Director!");
                }
            }
            else
            {
                Response.Write("You need to insert a name, please do it first!");
            }
        }
    }
}