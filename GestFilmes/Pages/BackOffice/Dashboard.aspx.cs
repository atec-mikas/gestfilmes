﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GestFilmes.DAL;
using GestFilmes.Libs;

namespace GestFilmes.Pages.BackOffice
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Access WebApi = WebHelper.CreateWebAPI();
            User user = Session["logIn"] as User;

            if (!WebHelper.isLoggedIn(user,TipoUser.Gestor))
            {
                Response.Redirect("~/Pages/Account/Login.aspx");
            }

            if (user.TipoUser == TipoUser.Gestor)
            {
                links.Visible = true;
            }
        }
    }
}