﻿<%@ Page Title="Dashboard" Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs"
    Inherits="GestFilmes.Pages.BackOffice.Estudios.Index" MasterPageFile="~/BackOffice.Master"%>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <div ID="linkCreateStudio" runat="server" class="mx-auto mt-4" visible="true" style="width:200px;">
        <asp:HyperLink runat='server' NavigateUrl='~/Pages/BackOffice/Estudios/Create.aspx' CssClass='btn btn-outline-secondary'>+ Adicionar novo estudio</asp:HyperLink>
    </div>
    <hr />
    <div class="container mt-5">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Lista de Estúdios</h5>
                <div ID="estudios" runat="server" class="estudios">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
