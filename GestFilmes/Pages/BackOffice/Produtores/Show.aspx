﻿<%@ Page Language="C#" MasterPageFile="~/BackOffice.Master" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="GestFilmes.Pages.BackOffice.Produtores.WebForm1" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <div ID="producers" runat="server" class="producers">
        <div class="container mt-5">
            <div class="card">
                <div class="card-body">
                    <div ID="nomeProdutor" runat="server" class="nomeProdutor">    
                    </div>
                    <div ID="Div1" runat="server" class="nomeProdutor  mx-auto">
                        <div class="float-right">
                            <asp:Button runat="server" OnClick="deleteProdutor" CssClass="btn btn-outline-danger" id="deleteProdutorButton" Text="Apagar produtor"/>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div> 
</asp:Content>