﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Input;

namespace GestFilmes
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
         

        protected void EndSession_Click(object sender, EventArgs e)
        {
            
            Session["logIn"] = null;
            Response.Redirect("~/Pages/Account/Login.aspx");
        }

        
    }
}