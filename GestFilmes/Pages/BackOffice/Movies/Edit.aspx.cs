﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GestFilmes.DAL;
using GestFilmes.Libs;
using System.IO;


namespace GestFilmes.Pages.BackOffice.Movies
{
    public partial class Edit : System.Web.UI.Page

    {

        private Filme filme;

        protected void Page_Load(object sender, EventArgs e)
        {

            User usr;
            usr = Session["logIn"] as User;
            Access WebApi = WebHelper.CreateWebAPI();
            if(Request.QueryString["id"] != null)
            {
                filme = WebApi.getFilme(Request.QueryString["id"]);
            }
            if (Session["logIn"] == null)
            {
                Response.Redirect("~/Pages/Account/Login.aspx");
            }
            if (usr.TipoUser == TipoUser.Registado && Session["logIn"] != null)
            {
                Response.Redirect("~/Pages/Account/Login.aspx");
            }
            if (usr.TipoUser == TipoUser.Gestor && Session["logIn"] != null)
            {

            }
            if (!IsPostBack)
            {
                Title.Text = filme.Titulo;
                Resume.Text = filme.Resumo;

                List<Ator> atores = WebApi.getAllAtores();
                DropDownListAtores.DataValueField = "ID";
                DropDownListAtores.DataTextField = "Nome";
                DropDownListAtores.DataSource = atores;
                DropDownListAtores.DataBind();
                List<Ator> filmeAtores = filme.Atores;
                foreach (ListItem item in DropDownListAtores.Items)
                {
                    if (filmeAtores.Any(p => p.ID == item.Value))
                    {
                        item.Selected = true;
                    }
                }
                List<Estudio> estudios = WebApi.getAllEstudios();
                DropDownListEstudios.DataValueField = "ID";
                DropDownListEstudios.DataTextField = "Nome";
                DropDownListEstudios.DataSource = estudios;
                DropDownListEstudios.DataBind();
                List<Estudio> filmeEstudios = filme.Estudios;
                foreach (ListItem item in DropDownListEstudios.Items)
                {
                    if (filmeEstudios.Any(p => p.ID == item.Value))
                    {
                        item.Selected = true;
                    }
                }

                List<Genero> generos = WebApi.getAllGeneros();
                DropDownListGeneros.DataValueField = "ID";
                DropDownListGeneros.DataTextField = "Nome";
                DropDownListGeneros.DataSource = generos;
                DropDownListGeneros.DataBind();
                //foreach (Genero genero in generos)
                //{
                //    DropDownListGeneros.Items.Add(genero);
                //}
                DropDownListGeneros.SelectedValue += filme.Genero.ID;


                List<Produtor> produtores = WebApi.getAllProdutores();
                DropDownListProdutores.DataValueField = "ID";
                DropDownListProdutores.DataTextField = "Nome";
                DropDownListProdutores.DataSource = produtores;
                DropDownListProdutores.DataBind();
                List<Produtor> filmeProdutores = filme.Produtores;
                foreach (ListItem item in DropDownListProdutores.Items)
                {
                    if(filmeProdutores.Any(p => p.ID == item.Value))
                    {
                        item.Selected = true;
                    }
                }
                List<Realizador> realizadores = WebApi.getAllRealizadores();
                DropDownListRealizadores.DataValueField = "ID";
                DropDownListRealizadores.DataTextField = "Nome";
                DropDownListRealizadores.DataSource = realizadores;
                DropDownListRealizadores.DataBind();
                List<Realizador> filmeRealizadores = filme.Realizadores;
                foreach (ListItem item in DropDownListRealizadores.Items)
                {
                    if (filmeRealizadores.Any(p => p.ID == item.Value))
                    {
                        item.Selected = true;
                    }
                }
            }
        }

        protected void Unnamed12_Click(object sender, EventArgs e)
        {
            if (FileUpload1.HasFile)
            {
                string fileName = "";
                string fileNameWithoutExt = Guid.NewGuid().ToString();
                fileName = fileNameWithoutExt + Path.GetExtension(FileUpload1.FileName);

                FileUpload1.SaveAs(Server.MapPath("~/Content/img/" + fileName));
                filme.Image = fileName;
            }
            Access WebApi = WebHelper.CreateWebAPI();
            filme.Titulo = Title.Text;
            filme.Resumo = Resume.Text;
            Genero genero;
            genero = WebApi.getGenero(DropDownListGeneros.SelectedValue);
            filme.Genero = genero;
            var result = WebApi.saveFilme(filme);
            foreach (ListItem item in DropDownListAtores.Items)
            {
                if (item.Selected)
                {
                    Ator ator;
                    ator = WebApi.getAtor(item.Value);
                    WebApi.addAtor(filme, ator);
                }
                if (!item.Selected)
                {
                    Ator ator;
                    ator = WebApi.getAtor(item.Value);
                    WebApi.removeAtor(filme, ator);
                }

            }
            foreach (ListItem item in DropDownListEstudios.Items)
            {
                if (item.Selected)
                {
                    Estudio estudio;
                    estudio = WebApi.getEstudio(item.Value);
                    WebApi.addEstudio(filme, estudio);
                }
                if (!item.Selected)
                {
                    Estudio estudio;
                    estudio = WebApi.getEstudio(item.Value);
                    WebApi.removeEstudio(filme, estudio);
                }

            }

            foreach (ListItem item in DropDownListProdutores.Items)
            {
                if (item.Selected)
                {
                    Produtor produtor;
                    produtor = WebApi.getProdutor(item.Value);
                    WebApi.addProdutor(filme, produtor);
                }
                if (!item.Selected)
                {
                    Produtor produtor;
                    produtor = WebApi.getProdutor(item.Value);
                    WebApi.removeProdutor(filme, produtor);
                }
            }
            foreach (ListItem item in DropDownListRealizadores.Items)
            {
                if (item.Selected)
                {
                    Realizador realizador;
                    realizador = WebApi.getRealizador(item.Value);
                    WebApi.addRealizador(filme, realizador);
                }
                if(!item.Selected)
                {
                    Realizador realizador;
                    realizador = WebApi.getRealizador(item.Value);
                    WebApi.removeRealizador(filme, realizador);
                }
            }


            Response.Redirect("~/Pages/FrontOffice/Movies/Show.aspx?id=" + filme.ID);
        }
    }
}