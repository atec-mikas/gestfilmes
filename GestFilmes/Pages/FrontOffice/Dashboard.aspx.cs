﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GestFilmes.DAL;
using GestFilmes.Libs;

namespace GestFilmes.Pages.FrontOffice
{
    public partial class Dashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            Access WebApi = WebHelper.CreateWebAPI();
            User user = Session["logIn"] as User;

            //if (!WebHelper.isLoggedIn(user))
            //{
            //    Response.Redirect("~/Pages/Account/Login.aspx");
            //}
            List<Filme> filmes = WebApi.getFilmes();
            if(filmes.Count() > 0)
            {
                foreach (Filme filme in filmes)
                {
                    Movies.InnerHtml += "<div class='responsive mb-3' runat='server'><div class='gallery' runat='server'><a href='Movies/Show.aspx?id=" + filme.ID + "'>" + "<img class='img-fluid' src='../../Content/img/" + filme.Image + "'" + " alt='' style='width:250px; height:250px;'><div class='desc'><span class='text-center'>" + filme.Titulo + "</span><br></a></div></div></div>";
                }
            }
            else
            {
                Movies.InnerHtml += "<div class='col-md-3 mx-auto'><span class='text-center'<h3>Não existem filmes na base de dados :)</h3></div>";
            }


        }
    }
}