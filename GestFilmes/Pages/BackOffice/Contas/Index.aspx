﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BackOffice.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="GestFilmes.Pages.BackOffice.Contas.Index" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container mt-5">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Lista de Contas</h5>
                <div ID="Dcontas" runat="server" class="contas">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
