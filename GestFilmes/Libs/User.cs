﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Hash = BCrypt.Net.BCrypt;

namespace GestFilmes.Libs
{
    public class User
    {
        private string _password;
        public string Username { get; set; }
        public TipoUser TipoUser { get; set; }
        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = Hash.HashPassword(value);
            }
        }

        public User()
        {
            _password = string.Empty;
            Username = string.Empty;
            TipoUser = TipoUser.Registado;
        }

        public User(DataRow data)
        {
            if(!data.IsNull("password")) 
                _password = data["password"] as string ?? "";
            if(!data.IsNull("username"))
                Username = data["username"] as string ?? "";
            if(!data.IsNull("tipo"))
            {
                TipoUser result = TipoUser.Registado; 
                TipoUser.TryParse(data["tipo"].ToString(), out result);
                TipoUser = result;
            }
        }

        public bool VerifyPassword(string password)
        {
            return Hash.Verify(password, _password);
        }
    }
}