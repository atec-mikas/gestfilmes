﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GestFilmes.DAL;
using GestFilmes.Libs;

namespace GestFilmes.Pages.BackOffice.Generos
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private Access WebApi;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            WebApi = WebHelper.CreateWebAPI();
           
            User usr = Session["logIn"] as User;
            if (!WebHelper.isLoggedIn(usr, TipoUser.Gestor))
            {
                Response.Redirect("~/Pages/Account/Login.aspx");
            }
            if (!WebHelper.isLoggedIn(usr))
            {
                Response.Redirect("~/Pages/FrontOffice/Dashboard.aspx");
            }
            if (Request.QueryString["id"] == null || WebApi.getGenero(Request.QueryString["id"])==null)
            {
                Response.Redirect("~/Pages/BackOffice/Dashboard.aspx");
            }
            Genero genero = WebApi.getGenero(Request.QueryString["id"]);
            nomeGenero.InnerHtml = "<span>Género número: " + genero.ID + "<br> Nome género: " + genero.Nome + "</span>"; 
        }
        protected void deleteGenero(object sender, EventArgs e)
        {
            Genero genero = WebApi.getGenero(Request.QueryString["id"]);
            if(WebApi.deleteGenero(genero) == true)
            {
                Response.Redirect("~/Pages/BackOffice/Generos/Index.aspx");
                Response.Write("Género " + genero.Nome + " eliminado com sucesso!");
            }else
            {
                Response.Write("Erro ao apagar género");
            }

        }
    }
}