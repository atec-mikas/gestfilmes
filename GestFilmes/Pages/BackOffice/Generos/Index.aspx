﻿<%@ Page Title="Dashboard" Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs"
    Inherits="GestFilmes.Pages.BackOffice.Generos.Index" MasterPageFile="~/BackOffice.Master"%>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <div ID="linkCreateGenre" runat="server" class="mx-auto mt-4" visible="true" style="width:200px;">
        <asp:HyperLink runat='server' NavigateUrl='~/Pages/BackOffice/Generos/Create.aspx' CssClass='btn btn-outline-secondary'>+ Adicionar novo genero</asp:HyperLink>
    </div>
    <hr />
         <div class="container mt-5">
             <div class="card">
                 <div class="card-body">
                     <h5 class="card-title">Lista de Géneros</h5>
                     <div ID="generos" runat="server" class="generos">
                     </div>
                 </div>
             </div>
         </div>
</asp:Content>
