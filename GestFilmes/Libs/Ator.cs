﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace GestFilmes.Libs
{
    public class Ator
    {
        private string _id;
        private string _nome;

        public Ator()
        {
            _id = string.Empty;
            _nome = string.Empty;
        }

        public Ator(DataRow data)
        {
            _id = data["id"] as string ?? "";
            _nome = data["nome"] as string ?? "";
        }

        private bool validarString(string s, int max = 100, int min = 3)
        {
            return !String.IsNullOrEmpty(s) && !String.IsNullOrWhiteSpace(s) && s.Count() <= max && s.Count() >= min;
        }

        public string ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
           
        }
        public string Nome
        {
            get
            {
                return _nome;
            }
            set
            {
                if (validarString(value))
                {
                    _nome = value;
                }
            }
        }
    }
}