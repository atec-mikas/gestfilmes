﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GestFilmes.DAL;
using GestFilmes.Libs;

namespace GestFilmes.Pages.BackOffice.Atores
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private Access WebApi;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            WebApi = WebHelper.CreateWebAPI();
           
            User usr = Session["logIn"] as User;
            if (!WebHelper.isLoggedIn(usr, TipoUser.Gestor))
            {
                Response.Redirect("~/Pages/Account/Login.aspx");
            }
            if (!WebHelper.isLoggedIn(usr))
            {
                Response.Redirect("~/Pages/FrontOffice/Dashboard.aspx");
            }
            if(Request.QueryString["id"] == null || WebApi.getAtor(Request.QueryString["id"]) == null)
            {
                Response.Redirect("~/Pages/BackOffice/Dashboard.aspx");
            }
            Ator ator = WebApi.getAtor(Request.QueryString["id"]);
            nomeAtor.InnerHtml = "<span>Ator número: " + ator.ID + "<br>Nome ator: " + ator.Nome + "</span>";

        }
        protected void deleteAtor(object sender, EventArgs e)
        {
            Ator ator = WebApi.getAtor(Request.QueryString["id"]);
            if(WebApi.deleteAtor(ator) == true)
            {
                Response.Redirect("~/Pages/BackOffice/Atores/Index.aspx");
                Response.Write("Ator" + ator.Nome + "eliminado com sucesso!");
            }else
            {
                Response.Write("Erro ao apagar ator");
            }

        }
    }
}