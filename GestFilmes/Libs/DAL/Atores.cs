﻿using GestFilmes.Libs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace GestFilmes.DAL
{
    internal static class Atores
    {
        private static DAL dal = new DAL("Ator", Properties.Settings.Default.DBPath);

        internal static Ator GetAtor(string id)
        {
            Ator ator = null;
            DataTable result = dal.find($"id,nome", $"where id = '{SQLHelper.EscapeSQL(id)}'");
            if (result.Rows.Count > 0)
            {
                ator = new Ator(result.Rows[0]);
            }
            return ator;
        }

        internal static List<Ator> GetAllAtores()
        {
            List<Ator> atores = new List<Ator>();
            DataTable result = dal.get($"id,nome");
            if (result.Rows.Count > 0)
            {
                foreach (DataRow ator in result.Rows)
                    atores.Add(new Ator(ator));
            }
            return atores;
        }

        internal static bool AtorExists(string id)
        {
            return dal.exists($"where id = '{SQLHelper.EscapeSQL(id)}'");
        }

        internal static bool SaveAtor(ref Ator ator)
        {
            List<KeyValuePair<string, object>> kv = new List<KeyValuePair<string, object>>();
            kv.Add(new KeyValuePair<string, object>("nome", ator.Nome));
            bool status = true;
            try
            {
                if (!AtorExists(ator.ID))
                {
                    string id;
                    do
                    {
                        id = Guid.NewGuid().ToString();
                    } while (AtorExists(id));
                    
                    kv.Add(new KeyValuePair<string, object>("id", id));
                    status = dal.insert(kv);
                    if (status)
                        ator.ID = id;
                }
                else
                {
                    status = dal.update(kv, $"where id='{SQLHelper.EscapeSQL(ator.ID)}'");
                }
            }
            catch
            {
                status = false;
            }
            return status;
        }

        internal static bool DeleteAtor(Ator ator)
        {
            DAL dalFilmeAtor = new DAL("Filme_Ator", Properties.Settings.Default.DBPath);
            return dalFilmeAtor.delete($"where ator_id='{SQLHelper.EscapeSQL(ator.ID)}'")
                    && dal.delete($"where id='{SQLHelper.EscapeSQL(ator.ID)}'");
        }
    }
}