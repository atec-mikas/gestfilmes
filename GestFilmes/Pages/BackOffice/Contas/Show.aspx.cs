﻿using GestFilmes.DAL;
using GestFilmes.Libs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GestFilmes.Pages.BackOffice.Contas
{
    public partial class Show : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Access WebApi = WebHelper.CreateWebAPI();

            User usr = Session["logIn"] as User;
            if (!WebHelper.isLoggedIn(usr, TipoUser.Gestor))
            {
                Response.Redirect("~/Pages/Account/Login.aspx");
            }
            if (!WebHelper.isLoggedIn(usr))
            {
                Response.Redirect("~/Pages/FrontOffice/Dashboard.aspx");
            }
            User user = null;
            if (Request.QueryString["id"] != null)
            {
                user = WebApi.getUser(Request.QueryString["id"]);
            }
            else
            {
                Response.Redirect("~/Pages/BackOffice/Dashboard.aspx");
            }
            if (user == null)
            {
                Response.Redirect("~/Pages/BackOffice/Dashboard.aspx");
            } else
            {
                DadosConta.InnerHtml = "<span>Nome Conta: " + user.Username + "<br> Tipo User: " + user.TipoUser.ToString() + "</span>";
            }
            Switcharoo.Text = (user.TipoUser == TipoUser.Registado ? "Promover a Gestor" : "Despromover a Resgistado" );
            Switcharoo.CssClass = (user.TipoUser == TipoUser.Registado ? "ml-2 btn btn-outline-warning" : "ml-2 btn btn-outline-danger");

        }

        protected void DeleteConta_Click(object sender, EventArgs e)
        {
            Access WebApi = WebHelper.CreateWebAPI();
            User usr = Session["logIn"] as User;
            User user = WebApi.getUser(Request.QueryString["id"]);

            if (user.TipoUser != TipoUser.Gestor && user.Username != usr.Username)
            {
                WebApi.deleteUser(user);
                Response.Redirect("~/Pages/BackOffice/Dashboard.aspx");
            }
        }

        protected void Switcharoo_Click(object sender, EventArgs e)
        {
            Access WebApi = WebHelper.CreateWebAPI();
            User usr = Session["logIn"] as User;
            User user = WebApi.getUser(Request.QueryString["id"]);
            if(user.Username != usr.Username)
            {
                user.TipoUser = (user.TipoUser == TipoUser.Registado ? TipoUser.Gestor : TipoUser.Registado);
                WebApi.saveUser(user);
            }
        }
    }
}