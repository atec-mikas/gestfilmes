﻿using GestFilmes.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace GestFilmes.Libs
{
    public class Comentario
    {
        public string ID { get; set; }
        public string Username { get; set; }

        public User User {
            get
            {
                Access webapi = WebHelper.CreateWebAPI();
                return webapi.getUser(Username);
            }
        }

        public string Conteudo { get; set; }
        public DateTime created_at { get; set; }

        public Comentario()
        {
            ID = "";
            Username = "";
            Conteudo = "";
            created_at = DateTime.MinValue;
        }

        public Comentario(DataRow data)
        {
            ID = data["id"] as string ?? "";
            Username = data["user_id"] as string ?? "";
            Conteudo = data["conteudo"] as string ?? "";
            created_at = data["created_at"] as DateTime? ?? DateTime.MinValue;
        }
    }
}