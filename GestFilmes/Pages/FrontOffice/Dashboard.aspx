﻿<%@ Page Title="Dashboard" Language="C#" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs"
    Inherits="GestFilmes.Pages.FrontOffice.Dashboard" MasterPageFile="~/Site.Master" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <div class"mt-5"></div>
    <hr />
    <div id="Movies" class="row justify-content-center mx-auto mt-4" runat="server">
    </div>
    <hr>
</asp:Content>
