﻿<%@ Page Language="C#" MasterPageFile="~/BackOffice.Master" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="GestFilmes.Pages.BackOffice.Estudios.WebForm1" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <div ID="studios" runat="server" class="studios">
         <div class="container mt-5">
            <div class="card">
                <div class="card-body">
                    <div ID="nomeEstudio" runat="server" class="nomeEstudio">
                    </div>
                    <div ID="Div1" runat="server" class="nomeEstudio">
                        <div class="float-right">
                            <asp:Button runat="server" OnClick="deleteEstudio" CssClass="btn btn-outline-danger" id="deleteEstudioButton" Text="Apagar estudio"/>
                        </div>
                    </div>
                </div>
            </div>
         </div>
    </div>
</asp:Content>